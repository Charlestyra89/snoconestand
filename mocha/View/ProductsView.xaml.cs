﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using mocha.ViewModel;
using mocha.Model;

namespace mocha.View
{
    /// <summary>
    /// Interaction logic for ProductsView.xaml
    /// </summary>
    public partial class ProductsView : UserControl
    {
        private ProductsViewModel vmProducts;
        private ProductViewModel vmProd;
        private RecipeViewModel vmRecipe;
        private CategoriesViewModel vmCategories;
        private RecipeView recView;
        private LotView lotView;
        private long productId = 0;
        
        public ProductsView(int catId)
        {
            InitializeComponent();
            vmProducts = new ProductsViewModel(catId);
            dgProducts.DataContext = vmProducts;

            vmRecipe = new RecipeViewModel();
            recView = new RecipeView(vmRecipe);
            vmProd = new ProductViewModel();
            vmCategories = new CategoriesViewModel();
            cmbCategories.DataContext = vmCategories;
            cmbCategories.ItemsSource = vmCategories.categories;
            lotView = new LotView();
            conLot.Content = lotView;
            if (vmProd.hasRecipe)
                conRecSup.Content = recView;
            else
                conRecSup.Content = null;
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            CheckBox box = (CheckBox)sender;
            if (box.IsChecked == true)
            {
                if (recView == null)
                    recView = new RecipeView(vmRecipe);
                conRecSup.Content = recView;
            }
            else
                conRecSup.Content = null;
        }

        private void dgProducts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgProducts.SelectedIndex > -1)
            {
                vmProd = (ProductViewModel)dgProducts.SelectedItem;
                productId = vmProd.id;
                this.DataContext = vmProd;
                cmbCategories.SelectedItem = vmCategories.categories.First(x => x.id == vmProd.categoryId);
                lotView = new LotView(productId);
                conLot.Content = lotView;
                vmRecipe = new RecipeViewModel(productId);
                if (vmProd.hasRecipe)
                {
                    recView = new RecipeView(vmRecipe);
                    conRecSup.Content = recView;
                }
                else
                    conRecSup.Content = null;
            }
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
