﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using mocha.ViewModel;

namespace mocha.View
{
    /// <summary>
    /// Interaction logic for ProductCategoryView.xaml
    /// </summary>
    public partial class ProductCategoryView : UserControl
    {
        private CategoriesViewModel categories;
        private MainWindow owner;
        public ProductCategoryView(MainWindow parent)
        {
            InitializeComponent();
            owner = parent;
            categories = new CategoriesViewModel();
            ExtGrid grid;
            TextBlock block;
            int row = 0;
            int column = 0;
            foreach (CategoryViewModel vmCat in categories.categories)
            {
                if (column == 4)
                {
                    column = 0;
                    row++;
                    gridCategories.Height += 170;
                    gridCategories.RowDefinitions.Add(new RowDefinition());
                }
                grid = new ExtGrid();
                block = new TextBlock();
                grid.Tag = vmCat.id;
                grid.display = vmCat.name;
                grid.Width = 150;
                grid.Height = 150;
                grid.Background = Brushes.Silver;
                grid.SetValue(Grid.RowProperty, row);
                grid.SetValue(Grid.ColumnProperty, column);
                block.Text = grid.display;
                block.TextWrapping = TextWrapping.Wrap;
                block.Foreground = Brushes.DarkBlue;
                block.Margin = new Thickness(10, 10, 0, 0);
                block.FontSize = 20;
                grid.Children.Add(block);
                grid.MouseUp += grid_MouseUp;
                gridCategories.Children.Add(grid);
                column++;
            }
        }

        private void grid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ExtGrid grid = (ExtGrid)sender;
            owner.appContent.Content = new ProductsView((int)grid.Tag);
        }
    }
}
