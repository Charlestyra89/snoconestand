﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using mocha.ViewModel;

namespace mocha.View
{
    /// <summary>
    /// Interaction logic for ModEmpView.xaml
    /// </summary>
    public partial class ModEmpView : Window
    {
        public EmployeesView owner;
        public EmployeeViewModel vmEmp;
        public ModEmpView(EmployeesView parent)
        {
            InitializeComponent();
            owner = parent;
            vmEmp = new EmployeeViewModel();
            this.DataContext = vmEmp;
        }

        public ModEmpView(EmployeesView parent, long empId)
        {
            InitializeComponent();
            owner = parent;
            vmEmp = new EmployeeViewModel(empId);
            this.DataContext = vmEmp;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            vmEmp.commitChanges();
            this.DataContext = null;
            this.DataContext = vmEmp;
            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
