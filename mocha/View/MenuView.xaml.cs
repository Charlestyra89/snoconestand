﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using mocha.Model;
using mocha.ViewModel;

namespace mocha.View
{
    /// <summary>
    /// Interaction logic for MenuView.xaml
    /// </summary>
    public partial class MenuView : UserControl
    {
        POSView owner;
        MenuGridViewModel vmMenuGrid;
        public MenuView(POSView parent)
        {
            InitializeComponent();
            owner = parent;
            generateGrid();
            
        }

        private void generateGrid()
        {
            vmMenuGrid = new MenuGridViewModel();
            var rows = vmMenuGrid.gridItems.Max(x => x.gridX);
            var cols = vmMenuGrid.gridItems.Max(x => x.gridY);
            RowDefinition rowDef;
            Grid itemPanel;
            SolidColorBrush stackBrush = new SolidColorBrush();
            stackBrush.Color = Colors.Snow;
            stackBrush.Opacity = 0.8;
            Image prodImage;
            TextBlock block;
            Viewbox vBox;
            MenuGridItem item;
            Border imageBorder;
            RowDefinition row1, row2;
            for (int i = 0; i <= rows; i++)
            {
                rowDef = new RowDefinition();
                rowDef.Height = new GridLength(150);
                gridMenu.RowDefinitions.Add(rowDef);
            }
            for (int i = 0; i <= cols; i++)
                gridMenu.ColumnDefinitions.Add(new ColumnDefinition());
            for (int i = 0; i <= rows; i++)
            {
                for (int j = 0; j <= cols; j++)
                {
                    
                    item = vmMenuGrid.gridItems.First(x => x.gridX == i && x.gridY == j);
                    ProductItem prod = ProductItem.getProduct(item.productId);
                    itemPanel = new Grid();
                    row1 = new RowDefinition();
                    row1.Height = new GridLength(80, GridUnitType.Star);
                    row2 = new RowDefinition();
                    row2.Height = new GridLength(20, GridUnitType.Star);
                    itemPanel.RowDefinitions.Add(row1);
                    itemPanel.RowDefinitions.Add(row2);
                    //itemPanel.Height = 150;
                    //itemPanel.Width = 150;
                    itemPanel.Margin = new Thickness(10);
                    itemPanel.Background = stackBrush;
                    
                    itemPanel.SetValue(Grid.RowProperty, i);
                    itemPanel.SetValue(Grid.ColumnProperty, j);
                    imageBorder = new Border();
                    imageBorder.Background = Brushes.White;
                    imageBorder.Margin = new Thickness(0, 0, 30, 0);
                    imageBorder.BorderBrush = Brushes.Black;
                    imageBorder.BorderThickness = new Thickness(2);
                    prodImage = new Image();
                    prodImage.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                    prodImage.SetValue(Grid.RowProperty, 0);
                    
                    var uriSource = new Uri(@"/mocha;component/View/Images/" + prod.image, UriKind.Relative);
                    prodImage.Source = new BitmapImage(uriSource);
                    //prodImage.Margin = new Thickness(10, 0, 40, 0);
                    imageBorder.Child = prodImage;
                    itemPanel.Children.Add(imageBorder);
                    block = new TextBlock();
                    
                    block.Text = prod.name;
                    block.FontSize = 20;
                    block.Margin = new Thickness(0, 10, 0, 0);
                    block.TextAlignment = TextAlignment.Right;
                    block.FontWeight = FontWeights.Bold;
                    vBox = new Viewbox();
                    vBox.SetValue(Grid.RowProperty, 1);
                    vBox.StretchDirection = StretchDirection.Both;
                    vBox.Stretch = Stretch.Uniform;
                    vBox.Child = block;
                    vBox.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
                    itemPanel.Children.Add(vBox);
                    itemPanel.Tag = item.productId;
                    itemPanel.MouseUp += itemPanel_MouseUp;
                    gridMenu.Children.Add(itemPanel);
                }
            }
        }

        private void itemPanel_MouseUp(object sender, MouseButtonEventArgs e)
        {
            int id = int.Parse(((Grid)sender).Tag.ToString());
            ProductItem prod = ProductItem.getProduct(id);
            ProductViewModel item = new ProductViewModel(owner.vmTrans);
            item.id = prod.id;
            item.name = prod.name;
            item.price = prod.price;
            item.modPrice = prod.price;
            item.qtyPrice = item.price;
            item.image = prod.image;
            owner.vmTrans.tryAdd(item);

        }
    }
}
