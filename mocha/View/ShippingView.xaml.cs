﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using mocha.ViewModel;

namespace mocha.View
{
    /// <summary>
    /// Interaction logic for ShippingView.xaml
    /// </summary>
    public partial class ShippingView : UserControl
    {
        MainWindow owner;
        ShippingViewModel vmShipping;
        public ShippingView(MainWindow parent)
        {
            InitializeComponent();
            vmShipping = new ShippingViewModel();
            this.DataContext = vmShipping;
            owner = parent;
            dgShipments.DataContext = vmShipping;
            dgShipments.ItemsSource = vmShipping.shipments;
            var list = dgShipments.ItemsSource;
        }

        private void btnShipments_MouseEnter(object sender, MouseEventArgs e)
        {
            Border bor = (Border)sender;
            TextBlock block = (TextBlock)bor.Child;
            bor.Background = Brushes.White;
            block.Foreground = Brushes.DimGray;

            
        }

        private void btnShipments_MouseLeave(object sender, MouseEventArgs e)
        {
            Border bor = (Border)sender;
            TextBlock block = (TextBlock)bor.Child;
            bor.Background = Brushes.DimGray;
            block.Foreground = Brushes.White;

           
        }

        private void btnShipments_MouseDown(object sender, MouseButtonEventArgs e)
        {
            btnShipments.Background = Brushes.White;
            ((TextBlock)btnShipments.Child).Foreground = Brushes.DimGray;
            btnLots.Background = Brushes.DimGray;
            ((TextBlock)btnLots.Child).Foreground = Brushes.White;
            this.DataContext = vmShipping;
            btnAddShip.Visibility = System.Windows.Visibility.Visible;
            btnModShip.Visibility = System.Windows.Visibility.Visible;
            btnDelShip.Visibility = System.Windows.Visibility.Visible;
            btnAddLot.Visibility = System.Windows.Visibility.Collapsed;
            btnModLot.Visibility = System.Windows.Visibility.Collapsed;
            btnDelLot.Visibility = System.Windows.Visibility.Collapsed;
            dgLots.Visibility = System.Windows.Visibility.Collapsed;
            dgShipments.Visibility = System.Windows.Visibility.Visible;
            stpSearch.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void btnLots_MouseDown(object sender, MouseButtonEventArgs e)
        {
            btnShipments.Background = Brushes.DimGray;
            ((TextBlock)btnShipments.Child).Foreground = Brushes.White;
            btnLots.Background = Brushes.White;
            ((TextBlock)btnLots.Child).Foreground = Brushes.DimGray;
            this.DataContext = vmShipping.vmProducts;
            btnAddShip.Visibility = System.Windows.Visibility.Collapsed;
            btnModShip.Visibility = System.Windows.Visibility.Collapsed;
            btnDelShip.Visibility = System.Windows.Visibility.Collapsed;
            btnAddLot.Visibility = System.Windows.Visibility.Visible;
            btnModLot.Visibility = System.Windows.Visibility.Visible;
            btnDelLot.Visibility = System.Windows.Visibility.Visible;
            dgLots.Visibility = System.Windows.Visibility.Visible;
            dgShipments.Visibility = System.Windows.Visibility.Collapsed;
            stpSearch.Visibility = System.Windows.Visibility.Visible;
        }

        private void btnAddShip_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void btnModShip_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void btnDelShip_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void btnAddLot_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void btnModLot_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void btnDelLot_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }
    }
}
