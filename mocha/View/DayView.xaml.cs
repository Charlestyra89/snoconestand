﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using mocha.ViewModel;
using mocha.Model;


namespace mocha.View
{
    /// <summary>
    /// Interaction logic for DayView.xaml
    /// </summary>
    public partial class DayView : UserControl
    {
        MainWindow owner;
        DateTime thisDay;

        public DayView(MainWindow parent, DateTime today)
        {
            InitializeComponent();
            owner = parent;
            thisDay = today;
            lblDate.Text = thisDay.ToString("dddd, MMM d");
            Border bor;
            SolidColorBrush transBlack = new SolidColorBrush();
            transBlack.Color = Colors.Black;
            transBlack.Opacity = 0.7;
            Border thisTime = (from r in grdWeek.Children.OfType<Border>()
                               where DateTime.Parse(((TextBlock)r.Child).Text) <= DateTime.Now && DateTime.Parse(((TextBlock)r.Child).Text) > DateTime.Now.AddMinutes(-30)
                               select r).ToList<Border>().First();
            thisTime.Background = Brushes.Green;
            for (int i = 0; i < grdWeek.RowDefinitions.Count; i++)
            {
                bor = new Border();
                bor.SetValue(Grid.RowProperty, i);
                bor.SetValue(Grid.ColumnProperty, 1);
                bor.SetValue(Grid.ColumnSpanProperty, 100);
                bor.BorderBrush = Brushes.White;
                bor.Background = transBlack;
                bor.BorderThickness = new Thickness(4, 0.5, 4, 0.5);

                grdWeek.Children.Add(bor);
            }

            scrWeek.ScrollToVerticalOffset(640.0);
        }

        private void Border_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ButtonBehavior.btnMouseDown(sender);
        }

        private void Border_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ButtonBehavior.btnMouseUp(sender);
            Border bor = (Border)sender;
            TextBlock txt = (TextBlock)bor.Child;
            DateTime newDate;
            if (txt.Text == "<")
            {
                newDate = thisDay.AddDays(-1);
                owner.appContent.Content = new DayView(owner, newDate);
            }
            else
            {
                newDate = thisDay.AddDays(1);
                owner.appContent.Content = new DayView(owner, newDate);
            }
        }

        private void btnWeek_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ButtonBehavior.btnMouseDown(sender);
            owner.appContent.Content = new WeekView(owner, DateTime.Now);
        }

        private void btnAddShift_MouseUp(object sender, MouseButtonEventArgs e)
        {
            AddShiftDialog addShift = new AddShiftDialog(this);
            addShift.ShowDialog();
            if (!addShift.valid)
                return;
            DateTime startTime = addShift.newStart;
            DateTime endTime = addShift.newEnd;
            int row = (startTime.Minute + (startTime.Hour * 60)) / 30;
            int duration = ((endTime - startTime).Minutes + ((endTime - startTime).Hours * 60)) / 30;
            int remainder = (startTime.Minute + (startTime.Hour * 60)) % 30;
            if (remainder > 0)
                remainder = 40 / (30 / remainder);
            int endRem = endTime.Minute % 30;
            if (endRem > 0)
                endRem = 40 / (30 / endRem);
            ColumnDefinition newCol = new ColumnDefinition();
            newCol.Width = new GridLength(60);
            grdWeek.ColumnDefinitions.Insert(grdWeek.ColumnDefinitions.Count-1, newCol);
            Canvas bor = new Canvas();
            bor.Width = 40;
            bor.Margin = new Thickness(0, remainder, 0, 1 - endRem);
            bor.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            bor.Background = Brushes.Red;
            bor.SetValue(Grid.RowProperty, row);
            bor.SetValue(Grid.ColumnProperty, grdWeek.ColumnDefinitions.Count - 2);
            bor.SetValue(Grid.RowSpanProperty, duration);
            bor.MouseDown += bor_MouseDown;
            bor.Tag = ScheduleItem.getItemId();
            grdWeek.Children.Add(bor);
            AddEmployeeToShift addEmp = new AddEmployeeToShift();
            addEmp.ShowDialog();
            ScheduleItem item = new ScheduleItem
            {
                id = (long)bor.Tag,
                startTime = startTime,
                endTime = endTime
            };
            if (addEmp.emp != "")
            {
                item.employeeId = addEmp.selectedId;
                TextBlock empName = new TextBlock();
                empName.Text = addEmp.emp;
                empName.Foreground = Brushes.White;
                empName.FontSize = 18.0;
                RotateTransform rot = new RotateTransform();
                rot.Angle = 90.0;
                rot.CenterY = 30;
                empName.RenderTransform = rot;
                bor.Children.Add(empName);
            }
            item.commitItem();
        }

        void bor_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                AddEmployeeToShift addEmp = new AddEmployeeToShift();
                addEmp.ShowDialog();
            }
        }
    }
}
