﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace mocha.View
{
    /// <summary>
    /// Interaction logic for TransNotesView.xaml
    /// </summary>
    public partial class TransNotesView : Window
    {
        private POSView owner;
        public TransNotesView(POSView parent, bool isNoSale)
        {
            InitializeComponent(); 
            owner = parent;
            if (isNoSale)
                txtInstructions.Text = "Enter reason for no sale:";
            else
                txtInstructions.Text = "Enter reason for hold:";
        }

        private void btnOK_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DialogResult = true;
            owner.vmTrans.notes = txtNotes.Text;
            this.Close();
        }

        private void btnCancel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.Left = ((POSView)owner).owner.Left + (((POSView)owner).owner.Width - this.ActualWidth) / 2;
            this.Top = ((POSView)owner).owner.Top + (((POSView)owner).owner.Height - this.ActualHeight) / 2;

        }
    }
}
