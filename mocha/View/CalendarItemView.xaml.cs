﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using mocha.ViewModel;

namespace mocha.View
{
    /// <summary>
    /// Interaction logic for CalendarItemView.xaml
    /// </summary>
    public partial class CalendarItemView : UserControl
    {
        CalendarItemViewModel vmItem;
        string parent;
        MainWindow owner;
        DateTime currentDate;

        public CalendarItemView(int _id, DateTime _date, string _subParent, MainWindow _parent)
        {
            InitializeComponent();
            parent = _subParent;
            owner = _parent;
            currentDate = _date;
            vmItem = new CalendarItemViewModel(_id, _date);
            this.DataContext = vmItem;
        }

        private void btnSave_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ButtonBehavior.btnMouseUp(sender);
            vmItem.description = txtDescription.Text;
            vmItem.description = txtDescription.Text;
            vmItem.startTime = (DateTime)tmeStartTime.Value;
            vmItem.endTime = (DateTime)tmeEndTime.Value;
            vmItem.paymentReceived = (bool)chkPaid.IsChecked;
            if (!vmItem.commitChanges())
                MessageBox.Show("Unable to save Calendar Item!");
            switch (parent)
            {
                case "mocha.View.CalendarView":
                    owner.appContent.Content = new CalendarView(owner, currentDate.Month, currentDate.Year);
                    break;
                case "mocha.View.WeekView":
                    owner.appContent.Content = new WeekView(owner, currentDate);
                    break;
                case "mocha.View.dayView":
                    owner.appContent.Content = new DayView(owner, currentDate);
                    break;
            }
        }

        private void btnCancel_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ButtonBehavior.btnMouseUp(sender);
            switch (parent)
            {
                case "mocha.View.CalendarView":
                    owner.appContent.Content = new CalendarView(owner, currentDate.Month, currentDate.Year);
                    break;
                case "mocha.View.WeekView":
                    owner.appContent.Content = new WeekView(owner, currentDate);
                    break;
                case "mocha.View.dayView":
                    owner.appContent.Content = new DayView(owner, currentDate);
                    break;
            }
        }

        private void Border_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ButtonBehavior.btnMouseDown(sender);
        }
    }
}
