﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using mocha.ViewModel;
using mocha.Model;

namespace mocha.View
{
    /// <summary>
    /// Interaction logic for RecipeView.xaml
    /// </summary>
    public partial class RecipeView : UserControl
    {
        int prevRowIndex = -1;
        public RecipeViewModel vmRecipe;
        public RecipeView(RecipeViewModel vmr)
        {
            InitializeComponent();
            vmRecipe = vmr;
            
            this.DataContext = vmRecipe;
        }

        private bool IsTheMouseOnTargetRow(Visual theTarget, GetDragDropPosition pos)
        {
            if (theTarget != null)
            {
                Rect posBounds = VisualTreeHelper.GetDescendantBounds(theTarget);
                Point theMousePos = pos((IInputElement)theTarget);
                return posBounds.Contains(theMousePos);
            }
            else
                return false;
        }

        private DataGridRow GetDataGridRowItem(int index, DataGrid dg)
        {
            if (dg.ItemContainerGenerator.Status != System.Windows.Controls.Primitives.GeneratorStatus.ContainersGenerated)
                return null;
            return dg.ItemContainerGenerator.ContainerFromIndex(index) as DataGridRow;
        }

        private int GetDataGridItemCurrentRowIndex(GetDragDropPosition pos, DataGrid dg)
        {
            int curIndex = -1;
            for (int i = 0; i < dg.Items.Count; i++)
            {
                DataGridRow itm = GetDataGridRowItem(i, dg);
                if (IsTheMouseOnTargetRow(itm, pos))
                {
                    curIndex = i;
                    break;
                }
            }
            return curIndex;
        }

        

        public delegate Point GetDragDropPosition(IInputElement theElement);

        private void gridRecipe_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            prevRowIndex = GetDataGridItemCurrentRowIndex(e.GetPosition, gridRecipe);
            if (prevRowIndex < 0)
                return;
            gridRecipe.SelectedIndex = prevRowIndex;
            RecipeStep selectedStep = gridRecipe.Items[prevRowIndex] as RecipeStep;
            if (selectedStep == null)
                return;
            DragDropEffects dragdropeffects = DragDropEffects.Move;

            if (DragDrop.DoDragDrop(gridRecipe, selectedStep, dragdropeffects) != DragDropEffects.None)
            {
                gridRecipe.SelectedItem = selectedStep;
            }
        }

        private void gridRecipe_Drop(object sender, DragEventArgs e)
        {
            if (prevRowIndex < 0)
                return;
            int index = this.GetDataGridItemCurrentRowIndex(e.GetPosition, gridRecipe);
            if (index < 0)
                return;
            if (index == prevRowIndex)
                return;
            if (index == gridRecipe.Items.Count - 1)
            {
                MessageBox.Show("This row cannot be drag and dropped");
                return;
            }
            RecipeViewModel recipeSteps = this.vmRecipe;
            RecipeStep movedStep = recipeSteps.steps[prevRowIndex];
            recipeSteps.steps.RemoveAt(prevRowIndex);
            recipeSteps.steps.Insert(index, movedStep);
        }

        private void gridIngredients_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            prevRowIndex = GetDataGridItemCurrentRowIndex(e.GetPosition, gridIngredients);
            if (prevRowIndex < 0)
                return;
            gridIngredients.SelectedIndex = prevRowIndex;
            IngredientItem selectedItem = gridIngredients.Items[prevRowIndex] as IngredientItem;
            if (selectedItem == null)
                return;
            DragDropEffects dragdropeffects = DragDropEffects.Move;

            if (DragDrop.DoDragDrop(gridIngredients, selectedItem, dragdropeffects) != DragDropEffects.None)
            {
                gridIngredients.SelectedItem = selectedItem;
            }
        }

        private void gridIngredients_Drop(object sender, DragEventArgs e)
        {
            if (prevRowIndex < 0)
                return;
            int index = this.GetDataGridItemCurrentRowIndex(e.GetPosition, gridIngredients);
            if (index < 0)
                return;
            if (index == prevRowIndex)
                return;
            if (index == gridIngredients.Items.Count - 1)
            {
                MessageBox.Show("This row cannot be drag and dropped");
                return;
            }
            RecipeViewModel recipeSteps = this.vmRecipe;
            IngredientItem movedItem = recipeSteps.ingredients[prevRowIndex];
            recipeSteps.ingredients.RemoveAt(prevRowIndex);
            recipeSteps.ingredients.Insert(index, movedItem);
        }

        private void gridIngredients_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            TextBox t = e.EditingElement as TextBox;
            string name = t.Text;
            int index = ((DataGrid)sender).SelectedIndex;
            IngredientsView ingrView = new IngredientsView(name, index, this);
            ingrView.ShowDialog();
            
        }
    }
}
