﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using mocha.ViewModel;

namespace mocha.View
{
    /// <summary>
    /// Interaction logic for PriceOverrideView.xaml
    /// </summary>
    public partial class PriceOverrideView : Window
    {
        POSView owner;
        ProductViewModel prod = null;
        public PriceOverrideView(POSView parent, ProductViewModel _prod)
        {
            InitializeComponent();
            owner = parent;
            prod = _prod;
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            if (prod != null)
            {
                prod.modPrice = Double.Parse(txtnewPrice.Text);
                owner.vmTrans.subtotal -= prod.qtyPrice;
                owner.vmTrans.subtotal += prod.modPrice;
            }
            else
            {
                owner.vmTrans.subtotal = double.Parse(txtnewPrice.Text);
            }
            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.Left = ((POSView)owner).owner.Left + (((POSView)owner).owner.Width - this.ActualWidth) / 2;
            this.Top = ((POSView)owner).owner.Top + (((POSView)owner).owner.Height - this.ActualHeight) / 2;

        }
    }
}
