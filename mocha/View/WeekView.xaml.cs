﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using mocha.ViewModel;

namespace mocha.View
{
    /// <summary>
    /// Interaction logic for WeekView.xaml
    /// </summary>
    public partial class WeekView : UserControl
    {
        MainWindow owner;
        DateTime start, end, weekDay;
        CalendarItemsViewModel vmCalItems;

        public WeekView(MainWindow parent, DateTime today)
        {
            InitializeComponent();
            
            owner = parent;
            int todayNum = getWeekDate(today.DayOfWeek);
            start = StartOfWeek(today, DayOfWeek.Sunday);
            weekDay = start;
            end = start.AddDays(6);
            lblDate.Text = String.Format("{0} to {1}", start.ToString("MMM-dd"), end.ToString("MMM-dd"));
            vmCalItems = new CalendarItemsViewModel(start, end);
            foreach (Border dayBor in grdCalendar.Children.OfType<Border>())
            {
                if (weekDay.Date == DateTime.Now.Date)
                    dayBor.BorderBrush = Brushes.Green;
                ((TextBlock)dayBor.Child).Text += " " + weekDay.Day;
                weekDay = weekDay.AddDays(1);
            }
            Border bor;
            SolidColorBrush transBlack = new SolidColorBrush();
            transBlack.Color = Colors.Black;
            transBlack.Opacity = 0.7;
            for (int i = 0; i < grdWeek.RowDefinitions.Count; i++)
            {
                for (int j = 1; j < grdWeek.ColumnDefinitions.Count; j++)
                {
                    bor = new Border();
                    bor.SetValue(Grid.RowProperty, i);
                    bor.SetValue(Grid.ColumnProperty, j);
                    bor.BorderBrush = Brushes.White;
                    bor.Background = transBlack;
                    bor.BorderThickness = new Thickness(4, 0.5, 4, 0.5);
                    DateTime current = start.AddDays(j - 1);
                    current = current.AddMinutes(i * 30);
                    bor.Tag = current.ToString();
                    bor.ToolTip = current.ToString();
                    grdWeek.Children.Add(bor);
                }
            }
            
            scrWeek.ScrollToVerticalOffset(640.0);
        }

        private void addEvents()
        {
            foreach (CalendarItemViewModel item in vmCalItems)
            {
                Border parent = (from r in grdCalendar.Children.OfType<Border>()
                                 where r.Tag != null && item.startTime >= DateTime.Parse(r.Tag.ToString()) && item.startTime < DateTime.Parse(r.Tag.ToString()).AddDays(1)
                                 select r).ToList<Border>().First();
                var parentGrd = (Grid)parent.Child;
                ScrollViewer scr = (ScrollViewer)parentGrd.Children[1];
                var grd = (Grid)scr.Content;
                RowDefinition newRow = new RowDefinition();
                newRow.Height = new GridLength(30);
                grd.RowDefinitions.Insert(0, newRow);
                var bor = new Border();
                bor.Tag = item.id;
                bor.Background = Brushes.Tomato;
                bor.Margin = new Thickness(3);
                bor.CornerRadius = new CornerRadius(15);
                var txt = new TextBlock();
                txt.Text = item.startTime.ToString("hh:mm tt") + " " + item.title;
                txt.TextWrapping = TextWrapping.Wrap;
                txt.Foreground = Brushes.White;
                txt.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                bor.Child = txt;
                bor.SetValue(Grid.RowProperty, grd.RowDefinitions.Count - 2);
                grd.Children.Add(bor);
            }
        }

        public static DateTime StartOfWeek(DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }
            return dt.AddDays(-1 * diff).Date;
        }


        private int getWeekDate(DayOfWeek day)
        {
            switch (day)
            {
                case DayOfWeek.Sunday:
                    return 1;
                case DayOfWeek.Monday:
                    return 2;
                case DayOfWeek.Tuesday:
                    return 3;
                case DayOfWeek.Wednesday:
                    return 4;
                case DayOfWeek.Thursday:
                    return 5;
                case DayOfWeek.Friday:
                    return 6;
                case DayOfWeek.Saturday:
                    return 7;
                default:
                    return 1;
            }
        }

        private void Border_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ButtonBehavior.btnMouseDown(sender);
        }

        private void Border_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ButtonBehavior.btnMouseUp(sender);
            Border bor = (Border)sender;
            TextBlock txt = (TextBlock)bor.Child;
            DateTime newDate;
            if (txt.Text == "<")
            {
                newDate = start.AddDays(-6);
                owner.appContent.Content = new WeekView(owner, newDate);
            }
            else
            {
                newDate = start.AddDays(7);
                owner.appContent.Content = new WeekView(owner, newDate);
            }
        }

        private void btnMonth_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ButtonBehavior.btnMouseDown(sender);
            owner.appContent.Content = new CalendarView(owner);
        }

        private void btnDay_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ButtonBehavior.btnMouseDown(sender);
            owner.appContent.Content = new DayView(owner, DateTime.Now);
        }
    }
}
