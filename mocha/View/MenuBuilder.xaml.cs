﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using mocha.ViewModel;
using mocha.Model;

namespace mocha.View
{
    /// <summary>
    /// Interaction logic for MenuBuilder.xaml
    /// </summary>
    public partial class MenuBuilder : UserControl
    {
        MainWindow owner;
        ProductsViewModel vmProds;
        bool captured = false;
        Grid box;
        MenuGridViewModel vmMenuGrid;

        public MenuBuilder(MainWindow parent)
        {
            InitializeComponent();
            vmProds = new ProductsViewModel();
            this.DataContext = vmProds;
            owner = parent;
            generateGrid();
        }

        private void generateGrid()
        {
            vmMenuGrid = new MenuGridViewModel();
            var rows = vmMenuGrid.gridItems.Max(x => x.gridX);
            var cols = vmMenuGrid.gridItems.Max(x => x.gridY);
            RowDefinition rowDef;
            Border bor;
            Grid menuItem;
            MenuGridItem item;
            for (int i = 0; i <= rows; i++)
            {
                rowDef = new RowDefinition();
                rowDef.Height = new GridLength(100);
                grdMenu.RowDefinitions.Add(rowDef);
            }
            for (int i = 0; i <= cols; i++)
                grdMenu.ColumnDefinitions.Add(new ColumnDefinition());
            for (int i = 0; i <= rows; i++)
            {
                for (int j = 0; j <= cols; j++)
                {
                    bor = new Border();
                    item = vmMenuGrid.gridItems.First(x => x.gridX == i && x.gridY == j);
                    ProductItem prod = ProductItem.getProduct(item.productId);
                    bor.BorderThickness = new Thickness(5, 5, 2, 2);
                    bor.BorderBrush = Brushes.White;
                    bor.Background = Brushes.Black;
                    bor.MaxWidth = 170;
                    bor.SetValue(Grid.RowProperty, i);
                    bor.SetValue(Grid.ColumnProperty, j);
                    grdMenu.Children.Add(bor);
                    menuItem = new Grid();
                    menuItem.Tag = prod.id;
                    RowDefinition row1 = new RowDefinition();
                    row1.Height = new GridLength(80, GridUnitType.Star);
                    RowDefinition row2 = new RowDefinition();
                    row2.Height = new GridLength(20, GridUnitType.Star);
                    menuItem.RowDefinitions.Add(row1);
                    menuItem.RowDefinitions.Add(row2);
                    Image img = new Image();
                    TextBlock txt = new TextBlock();
                    img.SetValue(Grid.RowProperty, 0);
                    var uriSource = new Uri(@"/WpfApplication3;component/View/" + prod.image, UriKind.Relative);
                    img.Source = new BitmapImage(uriSource);
                    txt.SetValue(Grid.RowProperty, 1);
                    txt.Text = prod.name;
                    txt.Foreground = Brushes.White;
                    txt.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                    menuItem.Children.Add(img);
                    menuItem.Children.Add(txt);
                    menuItem.MouseDown += menuItem_MouseDown;
                    bor.Child = menuItem;
                }
            }
            insertIncrementers();
        }

        private void insertIncrementers()
        {
            grdMenu.ColumnDefinitions.Add(new ColumnDefinition());
            RowDefinition newRow = new RowDefinition();
            newRow.Height = new GridLength(100);
            grdMenu.RowDefinitions.Add(newRow);
            Grid rowInc = new Grid();
            rowInc.SetValue(Grid.RowProperty, grdMenu.RowDefinitions.Count - 1);
            rowInc.SetValue(Grid.ColumnProperty, 0);
            Border btnIncrementRows = new Border();
            btnIncrementRows.Margin = new Thickness(2);
            btnIncrementRows.SetValue(Grid.RowProperty, 0);
            btnIncrementRows.Background = Brushes.Black;
            btnIncrementRows.BorderBrush = Brushes.White;
            btnIncrementRows.BorderThickness = new Thickness(5, 5, 2, 2);
            btnIncrementRows.CornerRadius = new CornerRadius(12);
            btnIncrementRows.MaxWidth = 110;
            btnIncrementRows.MouseDown += btnIncrementRows_MouseDown;
            btnIncrementRows.MouseEnter += btnIncrementRows_MouseEnter;
            btnIncrementRows.MouseLeave += btnIncrementRows_MouseLeave;

            TextBlock txtIncrementRows = new TextBlock();
            txtIncrementRows.Text = "+";
            txtIncrementRows.Foreground = Brushes.White;
            txtIncrementRows.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            txtIncrementRows.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            txtIncrementRows.FontSize = 28;

            Border btnDecrementRows = new Border();
            btnDecrementRows.Margin = new Thickness(2);
            btnDecrementRows.SetValue(Grid.RowProperty, 1);
            btnDecrementRows.Background = Brushes.Black;
            btnDecrementRows.BorderBrush = Brushes.White;
            btnDecrementRows.BorderThickness = new Thickness(5, 5, 2, 2);
            btnDecrementRows.CornerRadius = new CornerRadius(12);
            btnDecrementRows.MaxWidth = 110;
            btnDecrementRows.MouseDown += btnDecrementRows_MouseDown;
            btnDecrementRows.MouseEnter += btnIncrementRows_MouseEnter;
            btnDecrementRows.MouseLeave += btnIncrementRows_MouseLeave;

            TextBlock txtDecrementRows = new TextBlock();
            txtDecrementRows.Text = "-";
            txtDecrementRows.Foreground = Brushes.White;
            txtDecrementRows.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            txtDecrementRows.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            txtDecrementRows.FontSize = 28;


            rowInc.RowDefinitions.Add(new RowDefinition());
            rowInc.RowDefinitions.Add(new RowDefinition());
            btnIncrementRows.Child = txtIncrementRows;
            btnDecrementRows.Child = txtDecrementRows;
            rowInc.Children.Add(btnIncrementRows);
            rowInc.Children.Add(btnDecrementRows);
            rowInc.Margin = new Thickness(15);

            Grid colInc = new Grid();
            colInc.SetValue(Grid.ColumnProperty, grdMenu.ColumnDefinitions.Count - 1);
            colInc.SetValue(Grid.RowProperty, 0);
            Border btnIncrementCols = new Border();
            btnIncrementCols.Margin = new Thickness(2);
            btnIncrementCols.SetValue(Grid.RowProperty, 0);
            btnIncrementCols.Background = Brushes.Black;
            btnIncrementCols.BorderBrush = Brushes.White;
            btnIncrementCols.BorderThickness = new Thickness(5, 5, 2, 2);
            btnIncrementCols.CornerRadius = new CornerRadius(12);
            btnIncrementCols.MaxWidth = 110;
            btnIncrementCols.MouseDown += btnIncrementCols_MouseDown;
            btnIncrementCols.MouseEnter += btnIncrementRows_MouseEnter;
            btnIncrementCols.MouseLeave += btnIncrementRows_MouseLeave;

            TextBlock txtIncrementCols = new TextBlock();
            txtIncrementCols.Text = "+";
            txtIncrementCols.Foreground = Brushes.White;
            txtIncrementCols.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            txtIncrementCols.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            txtIncrementCols.FontSize = 28;

            Border btnDecrementCols = new Border();
            btnDecrementCols.Margin = new Thickness(2);
            btnDecrementCols.SetValue(Grid.RowProperty, 1);
            btnDecrementCols.Background = Brushes.Black;
            btnDecrementCols.BorderBrush = Brushes.White;
            btnDecrementCols.BorderThickness = new Thickness(5, 5, 2, 2);
            btnDecrementCols.CornerRadius = new CornerRadius(12);
            btnDecrementCols.MaxWidth = 110;
            btnDecrementCols.MouseDown += btnDecrementCols_MouseDown;
            btnDecrementCols.MouseEnter += btnIncrementRows_MouseEnter;
            btnDecrementCols.MouseLeave += btnIncrementRows_MouseLeave;

            TextBlock txtDecrementCols = new TextBlock();
            txtDecrementCols.Text = "-";
            txtDecrementCols.Foreground = Brushes.White;
            txtDecrementCols.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            txtDecrementCols.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            txtDecrementCols.FontSize = 28;

            colInc.RowDefinitions.Add(new RowDefinition());
            colInc.RowDefinitions.Add(new RowDefinition());
            btnIncrementCols.Child = txtIncrementCols;
            btnDecrementCols.Child = txtDecrementCols;
            colInc.Children.Add(btnIncrementCols);
            colInc.Children.Add(btnDecrementCols);
            colInc.Margin = new Thickness(15);
            grdMenu.Children.Add(rowInc);
            grdMenu.Children.Add(colInc);
        }

        private void btnIncrementRows_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Border thisBor = (Border)sender;
            Grid parent = (Grid)thisBor.Parent;
            RowDefinition newRow = new RowDefinition();
            newRow.Height = new GridLength(100);
            grdMenu.RowDefinitions.Add(newRow);
            Border bor;
            for (int i = 0; i < grdMenu.ColumnDefinitions.Count - 1; i++)
            {
                bor = new Border();
                bor.BorderThickness = new Thickness(5, 5, 2, 2);
                bor.BorderBrush = Brushes.White;
                bor.Background = Brushes.Black;
                bor.MaxWidth = 170;
                bor.SetValue(Grid.RowProperty, grdMenu.RowDefinitions.Count - 2);
                bor.SetValue(Grid.ColumnProperty, i);
                grdMenu.Children.Add(bor);
            }
            parent.SetValue(Grid.RowProperty, grdMenu.RowDefinitions.Count - 1);
        }

        private void btnDecrementRows_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Border thisBor = (Border)sender;
            Grid parent = (Grid)thisBor.Parent;
            if (grdMenu.RowDefinitions.Count > 2)
            {
                for (int i = 0; i < grdMenu.Children.Count; i++)
                {
                    try
                    {
                        if ((int)((Border)grdMenu.Children[i]).GetValue(Grid.RowProperty) == grdMenu.RowDefinitions.Count - 2)
                        {
                            grdMenu.Children.RemoveAt(i);
                            i--;
                        }
                    }
                    catch (Exception)
                    { }
                }
                parent.SetValue(Grid.RowProperty, grdMenu.RowDefinitions.Count - 2);
                grdMenu.RowDefinitions.RemoveAt(grdMenu.RowDefinitions.Count - 1);
            }
        }

        private void btnIncrementRows_MouseEnter(object sender, MouseEventArgs e)
        {

        }

        private void btnIncrementRows_MouseLeave(object sender, MouseEventArgs e)
        {

        }

        private void btnIncrementCols_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Border thisBor = (Border)sender;
            Grid parent = (Grid)thisBor.Parent;
            ColumnDefinition newCol = new ColumnDefinition();
            grdMenu.ColumnDefinitions.Add(newCol);
            Border bor;
            for (int i = 0; i < grdMenu.RowDefinitions.Count - 1; i++)
            {
                bor = new Border();
                bor.BorderThickness = new Thickness(5, 5, 2, 2);
                bor.BorderBrush = Brushes.White;
                bor.Background = Brushes.Black;
                bor.MaxWidth = 170;
                bor.SetValue(Grid.RowProperty, i);
                bor.SetValue(Grid.ColumnProperty, grdMenu.ColumnDefinitions.Count - 2);
                grdMenu.Children.Add(bor);
            }
            parent.SetValue(Grid.ColumnProperty, grdMenu.ColumnDefinitions.Count - 1);
        }

        private void btnDecrementCols_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Border thisBor = (Border)sender;
            Grid parent = (Grid)thisBor.Parent;
            if (grdMenu.ColumnDefinitions.Count > 2)
            {
                for (int i = 0; i < grdMenu.Children.Count; i++)
                {
                    try
                    {
                        if ((int)((Border)grdMenu.Children[i]).GetValue(Grid.ColumnProperty) == grdMenu.ColumnDefinitions.Count - 2)
                        {
                            grdMenu.Children.RemoveAt(i);
                            i--;
                        }
                    }
                    catch (Exception)
                    { }
                }
                parent.SetValue(Grid.ColumnProperty, grdMenu.ColumnDefinitions.Count - 2);
                grdMenu.ColumnDefinitions.RemoveAt(grdMenu.ColumnDefinitions.Count - 1);
            }
        }

        private void btnSave_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.vmMenuGrid.gridItems.Clear();
            bool result;
            foreach (UIElement child in grdMenu.Children)
            {
                Border bor;
                Grid grd;
                try
                {
                    bor = (Border)child;
                    grd = (Grid)bor.Child;
                    int row = Grid.GetRow(bor);
                    int col = Grid.GetColumn(bor);
                    if (row < grdMenu.RowDefinitions.Count - 1 && col < grdMenu.ColumnDefinitions.Count - 1)
                    {
                        string tag = (grd == null || grd.Tag == null ? "-1" : grd.Tag.ToString());
                        vmMenuGrid.gridItems.Add(new MenuGridItem(long.Parse(tag), row, col));
                    }
                }
                catch (Exception)
                { }
            }
            result = vmMenuGrid.commitItems();
            if (!result)
                MessageBox.Show("Unable to save menu grid!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void btnCancel_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void canContent_LayoutUpdated(object sender, EventArgs e)
        {
            scrMenu.Width = canContent.ActualWidth - 300;
        }


        private void dgProducts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgProducts.SelectedIndex != -1)
            {
                ProductViewModel vmSelected = (ProductViewModel)dgProducts.SelectedItem;
                createDragBox(vmSelected);
                dgProducts.SelectedIndex = -1;
            }
        }

        private void createDragBox(ProductViewModel prod)
        {
            box = new Grid();
            box.Width = 100;
            box.Height = 100;
            box.Background = Brushes.Gray;
            box.Opacity = 0.6;
            box.MouseMove += box_MouseMove;
            box.MouseUp += box_MouseUp;
            box.SetValue(Canvas.LeftProperty, Mouse.GetPosition(canContent).X - 50);
            box.SetValue(Canvas.TopProperty, Mouse.GetPosition(canContent).Y - 50);
            box.Tag = prod.id;
            if (prod.image != "")
            {
                Image img = new Image();
                var uriSource = new Uri(@"/WpfApplication3;component/View/" + prod.image, UriKind.Relative);
                img.Source = new BitmapImage(uriSource);
                box.Children.Add(img);
            }
            captured = true;
            canContent.Children.Add(box);
            dgProducts.IsEnabled = false;
        }

        void box_MouseUp(object sender, MouseButtonEventArgs e)
        {
            captured = false;
            dgProducts.IsEnabled = true;

            int row = -1, col = -1;
            if (grdMenu != null)
            {
                var pos = e.GetPosition(grdMenu);
                var temp = pos.X;
                for (var i = 0; i < grdMenu.ColumnDefinitions.Count; i++)
                {
                    var colDef = grdMenu.ColumnDefinitions[i];
                    temp -= colDef.ActualWidth;
                    if (temp <= -1)
                    {
                        col = i;
                        break;
                    }
                }

                temp = pos.Y;
                for (var i = 0; i < grdMenu.RowDefinitions.Count; i++)
                {
                    var rowDef = grdMenu.RowDefinitions[i];
                    temp -= rowDef.ActualHeight;
                    if (temp <= -1)
                    {
                        row = i;
                        break;
                    }
                }
            }
            if (row != -1 && col != -1 && row < grdMenu.RowDefinitions.Count - 1 && col < grdMenu.ColumnDefinitions.Count - 1)
            {
                Border bor = grdMenu.Children.Cast<UIElement>().FirstOrDefault(x => Grid.GetColumn(x) == col && Grid.GetRow(x) == row) as Border;
                if (bor != null)
                {
                    bor.Child = null;
                    long id = long.Parse(((Grid)sender).Tag.ToString());
                    ProductItem prod = ProductItem.getProduct(id);
                    Grid menuItem = new Grid();
                    menuItem.Tag = id;
                    RowDefinition row1 = new RowDefinition();
                    row1.Height = new GridLength(80, GridUnitType.Star);
                    RowDefinition row2 = new RowDefinition();
                    row2.Height = new GridLength(20, GridUnitType.Star);
                    menuItem.RowDefinitions.Add(row1);
                    menuItem.RowDefinitions.Add(row2);
                    Image img = new Image();
                    TextBlock txt = new TextBlock();
                    img.SetValue(Grid.RowProperty, 0);
                    var uriSource = new Uri(@"/WpfApplication3;component/View/" + prod.image, UriKind.Relative);
                    img.Source = new BitmapImage(uriSource);
                    txt.SetValue(Grid.RowProperty, 1);
                    txt.Text = prod.name;
                    txt.Foreground = Brushes.White;
                    txt.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                    menuItem.Children.Add(img);
                    menuItem.Children.Add(txt);
                    menuItem.MouseDown += menuItem_MouseDown;
                    bor.Child = menuItem;
                }
            }

            canContent.Children.Remove(box);
        }

        void menuItem_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Grid grd = (Grid)sender;
            long id = long.Parse(((Grid)sender).Tag.ToString());
            ProductViewModel vmProd = new ProductViewModel(id);
            createDragBox(vmProd);
            Border bor = (Border)grd.Parent;
            bor.Child = null;
        }

        void box_MouseMove(object sender, MouseEventArgs e)
        {
            if (captured)
            {
                box.SetValue(Canvas.LeftProperty, Mouse.GetPosition(canContent).X - 50);
                box.SetValue(Canvas.TopProperty, Mouse.GetPosition(canContent).Y - 50);
            }

        }

        private void btnClear_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Border bor;
            for (int i = 0; i < grdMenu.RowDefinitions.Count - 1; i++)
            {
                for (int j = 0; j < grdMenu.ColumnDefinitions.Count - 1; j++)
                {
                    bor = grdMenu.Children.Cast<UIElement>().FirstOrDefault(x => Grid.GetColumn(x) == j && Grid.GetRow(x) == i) as Border;
                    if (bor != null)
                    {
                        bor.Child = null;
                    }
                }
            }
        }

    }
}
