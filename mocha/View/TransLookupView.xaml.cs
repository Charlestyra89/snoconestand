﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using mocha.ViewModel;

namespace mocha.View
{
    /// <summary>
    /// Interaction logic for TransLookupView.xaml
    /// </summary>
    public partial class TransLookupView : Window
    {
        private POSView owner;
        private TransListViewModel vmList;
        public TransLookupView(POSView parent)
        {
            InitializeComponent();
            vmList = new TransListViewModel(false, false, false, true);
            this.DataContext = vmList;
            owner = parent;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.Left = ((POSView)owner).owner.Left + (((POSView)owner).owner.Width - this.ActualWidth) / 2;
            this.Top = ((POSView)owner).owner.Top + (((POSView)owner).owner.Height - this.ActualHeight) / 2;

        }

        private void DataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DataGrid dg = (DataGrid)sender;
            TransactionViewModel vmTrans = (TransactionViewModel)dg.SelectedItem;
            long selectedId = vmTrans.id;
            owner.vmTrans = vmTrans.getTransaction(selectedId);
        }

        private void btnCancel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }

        private void btnOK_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }

        private void btnAll_MouseDown(object sender, MouseButtonEventArgs e)
        {
            vmList = new TransListViewModel(false, false, false, true);
            this.DataContext = vmList;
        }

        private void btnCompleted_MouseDown(object sender, MouseButtonEventArgs e)
        {
            vmList = new TransListViewModel(true, false, false, false);
            this.DataContext = vmList;
        }

        private void btnOnHold_MouseDown(object sender, MouseButtonEventArgs e)
        {
            vmList = new TransListViewModel(false, true, false, false);
            this.DataContext = vmList;
        }

        private void btnNoSales_MouseDown(object sender, MouseButtonEventArgs e)
        {
            vmList = new TransListViewModel(false, false, true, false);
            this.DataContext = vmList;
        }
    }
}
