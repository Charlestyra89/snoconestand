﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using mocha.Model;
using mocha.ViewModel;

namespace mocha.View
{
    /// <summary>
    /// Interaction logic for POSView.xaml
    /// </summary>
    public partial class POSView : UserControl
    {
        public TransactionViewModel vmTrans;

        private MenuView menu;
        private RegisterView register;
        public MainWindow owner;

        public POSView(MainWindow parent)
        {
            InitializeComponent();
            owner = parent;
            menu = new MenuView(this);
            conMainPane.Content = menu;
            vmTrans = new TransactionViewModel();
            register = new RegisterView(this, ref vmTrans);
            this.DataContext = vmTrans;
            dgTransaction.ItemsSource = vmTrans.products;
        }

        private void Border_MouseEnterGray(object sender, MouseEventArgs e)
        {
            Border bor = (Border)sender;
            bor.Background = Brushes.White;
            bor.Opacity = 1;
        }

        private void Border_MouseEnterBlack(object sender, MouseEventArgs e)
        {
            Border bor = (Border)sender;
            bor.Background = Brushes.White;
            ((TextBlock)bor.Child).Foreground = Brushes.Black;
        }

        private void LookUp_MouseEnterBlack(object sender, MouseEventArgs e)
        {
            Border bor = (Border)sender;
            bor.Background = Brushes.White;
            ((TextBlock)((Viewbox)bor.Child).Child).Foreground = Brushes.Black;
        }

        private void Border_MouseLeaveGray(object sender, MouseEventArgs e)
        {
            Border bor = (Border)sender;
            bor.Background = Brushes.LightGray;
            bor.Opacity = 0.8;
        }

        private void Border_MouseLeaveBlack(object sender, MouseEventArgs e)
        {
            Border bor = (Border)sender;
            bor.Background = Brushes.Black;
            ((TextBlock)bor.Child).Foreground = Brushes.White;
        }

        private void LookUp_MouseLeaveBlack(object sender, MouseEventArgs e)
        {
            Border bor = (Border)sender;
            bor.Background = Brushes.Black;
            ((TextBlock)((Viewbox)bor.Child).Child).Foreground = Brushes.White;
        }

        private void dgTransaction_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ProductViewModel selected = (ProductViewModel)dgTransaction.SelectedItem;
            if (selected.qty > 1)
            {
                selected.qty--;
                vmTrans.subtotal -= selected.price;
            }
            else
            {
                vmTrans.subtotal -= selected.price;
                vmTrans.products.Remove(selected);
            }
        }

        private void btnPurchase_MouseDown(object sender, MouseButtonEventArgs e)
        {
            conMainPane.Content = register;
        }

        private void btHold_MouseDown(object sender, MouseButtonEventArgs e)
        {
            bool isOnHold = true;
            bool isNoSale = false;
            TransNotesView tnView = new TransNotesView(this, false);
            tnView.ShowDialog();
            if (tnView.DialogResult == true)
                commitTransaction(isOnHold, isNoSale);
        }

        private void btnNoSale_MouseDown(object sender, MouseButtonEventArgs e)
        {
            bool isOnHold = true;
            bool isNoSale = false;
            TransNotesView tnView = new TransNotesView(this, true);
            tnView.ShowDialog();
            if (tnView.DialogResult == true)
                commitTransaction(isOnHold, isNoSale);
        }

        public void commitTransaction(bool onHold, bool noSale)
        {
            bool trycommit = vmTrans.commitTrans(onHold, noSale);
            if (trycommit)
                MessageBox.Show("Transaction Commited!");
            else
                MessageBox.Show("Failed to commit Transaction!");
            menu = new MenuView(this);
            conMainPane.Content = menu;
            vmTrans = new TransactionViewModel();
            register = new RegisterView(this, ref vmTrans);
            this.DataContext = vmTrans;
            dgTransaction.ItemsSource = vmTrans.products;
            conMainPane.Content = menu;
        }

        private void btnLookup_MouseDown(object sender, MouseButtonEventArgs e)
        {
            TransLookupView lookupView = new TransLookupView(this);
            lookupView.ShowDialog();
        }

        private void btnOverride_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ManagerApprovalView manapView = new ManagerApprovalView(this, null);
            manapView.ShowDialog();
        }

        private void dgTransaction_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            DependencyObject dep = (DependencyObject)e.OriginalSource;
            while ((dep != null) && !(dep is DataGridCell))
            {
                dep = VisualTreeHelper.GetParent(dep);
            }
            if (dep == null) return;

            if (dep is DataGridCell)
            {
                DataGridCell cell = dep as DataGridCell;
                cell.Focus();

                while ((dep != null) && !(dep is DataGridRow))
                {
                    dep = VisualTreeHelper.GetParent(dep);
                }
                DataGridRow row = dep as DataGridRow;
                dgTransaction.SelectedItem = row.DataContext;
            }

            ProductViewModel selected = (ProductViewModel)dgTransaction.SelectedItem;
            ManagerApprovalView manapView = new ManagerApprovalView(this, selected);
            manapView.ShowDialog();
        }
    }
}
