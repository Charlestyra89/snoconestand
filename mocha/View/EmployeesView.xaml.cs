﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using mocha.ViewModel;

namespace mocha.View
{
    /// <summary>
    /// Interaction logic for EmployeesView.xaml
    /// </summary>
    public partial class EmployeesView : UserControl
    {
        private EmployeesViewModel vmEmps;
        private EmployeeViewModel vmEmployee;
        public EmployeesView()
        {
            InitializeComponent();
            vmEmps = new EmployeesViewModel();
            vmEmployee = new EmployeeViewModel();
            this.DataContext = vmEmployee;
            dgEmployees.DataContext = vmEmps;
            dgEmployees.ItemsSource = vmEmps.employees;
        }

        private void dgEmployees_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid dg = (DataGrid)sender;
            if (dg.SelectedIndex > -1)
            {
                vmEmployee = (EmployeeViewModel)dg.SelectedItem;
                this.DataContext = vmEmployee;
            }
        }

        private void btnAddNew_MouseUp(object sender, MouseButtonEventArgs e)
        {
            this.DataContext = new EmployeeViewModel();
        }

        private void btnRemove_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (dgEmployees.SelectedIndex < 0)
                return;
            EmployeeViewModel vmEmp = (EmployeeViewModel)dgEmployees.SelectedItem;
            string result = vmEmp.deleteEmployee();
            vmEmps = new EmployeesViewModel();
            dgEmployees.DataContext = vmEmps;
            dgEmployees.ItemsSource = vmEmps.employees;
            if (result != "")
                MessageBox.Show(result);
        }

        private void btnSave_MouseUp(object sender, MouseButtonEventArgs e)
        {
            dgEmployees.Focus();
            vmEmployee.commitChanges();
            vmEmps = new EmployeesViewModel();
            dgEmployees.DataContext = vmEmps;
            dgEmployees.ItemsSource = vmEmps.employees;
        }
    }
}
