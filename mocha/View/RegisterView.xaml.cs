﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using mocha.ViewModel;
using mocha.Model;

namespace mocha.View
{
    /// <summary>
    /// Interaction logic for RegisterView.xaml
    /// </summary>
    public partial class RegisterView : UserControl
    {
        POSView owner;
        public RegisterView(POSView parent, ref TransactionViewModel vmTrans)
        {
            InitializeComponent();
            this.DataContext = vmTrans;
            loadButtons();
            owner = parent;
        }
        private void loadButtons()
        {
            TenderTypes types = TenderTypes.getTypes();
            Border btnBorder;
            TextBlock btnBlock;
            Viewbox vwBox;
            int gridRow = 1;
            int gridCol = 0;
            foreach (TenderType type in types)
            {
                btnBorder = new Border();
                btnBlock = new TextBlock();
                vwBox = new Viewbox();
                vwBox.Stretch = Stretch.Uniform;
                vwBox.StretchDirection = StretchDirection.Both;
                btnBlock.Text = type.description;
                btnBlock.FontSize = 26;
                btnBlock.Foreground = Brushes.White;
                btnBlock.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                attachEvent(ref btnBlock);
                if (gridCol == 2)
                {
                    gridCol = 0;
                    gridRow++;
                }
                vwBox.SetValue(Grid.RowProperty, gridRow);
                vwBox.SetValue(Grid.ColumnProperty, gridCol++);
                btnBorder.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
                btnBorder.Background = Brushes.Black;
                btnBorder.BorderBrush = Brushes.White;
                btnBorder.BorderThickness = new Thickness(3);
                btnBorder.CornerRadius = new CornerRadius(10);
                btnBorder.Width = 120;
                btnBorder.Margin = new Thickness(5);
                btnBorder.Child = btnBlock;
                vwBox.Child = btnBorder;
                grdPaymentType.Children.Add(vwBox);
            }
        }

        private void attachEvent(ref TextBlock currTxt)
        {
            switch (currTxt.Text)
            {
                case "PayPal":
                    currTxt.MouseDown += btnPaypalTender_MouseDown;
                    break;
                case "Cash":
                    currTxt.MouseDown += btnCashTender_MouseDown;
                    break;
                case "Credit":
                    currTxt.MouseDown += btnCreditTender_MouseDown;
                    break;
                case "Debit":
                    currTxt.MouseDown += btnDebitTender_MouseDown;
                    break;
                case "Check":
                    currTxt.MouseDown += btnCheckTender_MouseDown;
                    break;
            }
        }
        private void btn_MouseEnter(object sender, MouseEventArgs e)
        {
            Border bor = (Border)sender;
            bor.Background = Brushes.White;
            ((TextBlock)bor.Child).Foreground = Brushes.Black;
        }

        private void btn_MouseLeave(object sender, MouseEventArgs e)
        {
            Border bor = (Border)sender;
            bor.Background = Brushes.Black;
            ((TextBlock)bor.Child).Foreground = Brushes.White;
        }

        private void btn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Border bor = (Border)sender;
            string value = ((TextBlock)bor.Child).Text;
            if (value == "B")
            {
                if (txtAmountToTender.Text != "$0.00")
                {
                    string substring = txtAmountToTender.Text.Substring(1);
                    substring = substring.Replace(".", "");
                    substring = string.Concat("0", substring);
                    substring = substring.Substring(0, substring.Length - 1);
                    substring = substring.Insert(substring.Length - 2, ".");
                    txtAmountToTender.Text = double.Parse(substring).ToString("C");
                }
            }
            else
            {
                string substring = txtAmountToTender.Text.Substring(1);
                substring = substring.Replace(".", "");
                substring = string.Concat(substring, value);
                substring = substring.Insert(substring.Length - 2, ".");
                txtAmountToTender.Text = double.Parse(substring).ToString("C");

            }
        }

        private void btnCashTender_MouseDown(object sender, MouseButtonEventArgs e)
        {
            double tenderAmount;
            string data = txtAmountToTender.Text.Replace("$", "");
            bool result = double.TryParse(data, out tenderAmount);
            if (result)
            {
                txtAmountToTender.BorderBrush = Brushes.Transparent;
                txtAmountToTender.BorderThickness = new Thickness(0);
                ((TransactionViewModel)this.DataContext).amountPaid += tenderAmount;
                txtAmountToTender.Text = "";
                if (((TransactionViewModel)this.DataContext).amountDue <= 0)
                {
                    ChangeDueView vChange = new ChangeDueView(owner, ((TransactionViewModel)this.DataContext).amountDue);
                    vChange.ShowDialog();
                }
            }
            else
            {
                txtAmountToTender.BorderBrush = Brushes.Red;
                txtAmountToTender.BorderThickness = new Thickness(3);
            }
        }

        private void btnDebitTender_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void btnCreditTender_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void btnCheckTender_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void btnPaypalTender_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }
    }
}
