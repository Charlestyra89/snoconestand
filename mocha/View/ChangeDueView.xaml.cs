﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace mocha.View
{
    /// <summary>
    /// Interaction logic for ChangeDueView.xaml
    /// </summary>
    public partial class ChangeDueView : Window
    {
        POSView owner;
        public ChangeDueView(POSView parent, double change)
        {
            InitializeComponent();
            owner = parent;
            this.txtChange.Text = change.ToString("C");
        }

        private void btnTendered_MouseDown(object sender, MouseButtonEventArgs e)
        {
            owner.commitTransaction(false, false);
            this.Close();
        }

        private void btnTendered_MouseEnter(object sender, MouseEventArgs e)
        {
            btnTendered.Background = Brushes.White;
        }

        private void btnTendered_MouseLeave(object sender, MouseEventArgs e)
        {
            btnTendered.Background = Brushes.Transparent;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.Left = ((POSView)owner).owner.Left + (((POSView)owner).owner.Width - this.ActualWidth) / 2;
            this.Top = ((POSView)owner).owner.Top + (((POSView)owner).owner.Height - this.ActualHeight) / 2;

        }
    }
}
