﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using mocha.ViewModel;
using mocha.Model;

namespace mocha.View
{
    /// <summary>
    /// Interaction logic for LotView.xaml
    /// </summary>
    public partial class LotView : UserControl
    {
        private long prodId;
        private LotViewModel vmLots;

        public LotView()
        {
            InitializeComponent();
            vmLots = new LotViewModel();
            this.DataContext = vmLots.lots;
        }

        public LotView(long prodId)
        {
            InitializeComponent();
            this.prodId = prodId;
            vmLots = new LotViewModel(this.prodId);
            this.DataContext = vmLots.lots;
        }

        private void dgProductLots_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            foreach (var lot in vmLots.lots)
            {
                lot.use = false;
            }
            ((ProductLot)dgProductLots.SelectedItem).use = true;
        }

       
    }
}
