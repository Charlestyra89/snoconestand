﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace mocha.View
{
    public class ExtGrid : Grid
    {
        public string display { get; set; }
    }

    public class ExtContentControl : ContentControl
    {

        static ExtContentControl()
        {

            ContentProperty.OverrideMetadata(typeof(ExtContentControl),

                new FrameworkPropertyMetadata(

                    new PropertyChangedCallback(OnContentChanged)));

        }



        private static void OnContentChanged(DependencyObject d,

            DependencyPropertyChangedEventArgs e)
        {

            ExtContentControl mcc = d as ExtContentControl;

            if (mcc.ContentChanged != null)
            {

                DependencyPropertyChangedEventArgs args

                    = new DependencyPropertyChangedEventArgs(

                        ContentProperty, e.OldValue, e.NewValue);

                mcc.ContentChanged(mcc, args);

            }

        }



        public event DependencyPropertyChangedEventHandler ContentChanged;

    }

}
