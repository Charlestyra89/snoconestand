﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using mocha.ViewModel;

namespace mocha.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public DashboardView dashView;
        public TimeClockViewModel vmClock;
        private TimeClockView clockView;
        private POSView posView;

        public MainWindow()
        {
            InitializeComponent();
            vmClock = new TimeClockViewModel();
            dashView = new DashboardView(this);
            clockView = new TimeClockView(this);
            posView = new POSView(this);
            appContent.Content = dashView;
        }

        

        private void btnHome_MouseDown(object sender, MouseButtonEventArgs e)
        {
            appContent.Content = dashView;
        }

        private void btnPosFunctions_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Border bor = (Border)sender;
            bor.ContextMenu.IsEnabled = true;
            bor.ContextMenu.PlacementTarget = bor;
            bor.ContextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
            bor.ContextMenu.IsOpen = true;
        }

        private void btnTimeClockFunctions_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Border bor = (Border)sender;
            bor.ContextMenu.IsEnabled = true;
            bor.ContextMenu.PlacementTarget = bor;
            bor.ContextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
            bor.ContextMenu.IsOpen = true;
        }

        private void btnAdminFunctions_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Border bor = (Border)sender;
            bor.ContextMenu.IsEnabled = true;
            bor.ContextMenu.PlacementTarget = bor;
            bor.ContextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
            bor.ContextMenu.IsOpen = true;
        }

        private void btnHome_MouseEnter(object sender, MouseEventArgs e)
        {
            ButtonBehavior.btnMouseEnter(sender);
        }

        private void btnHome_MouseLeave(object sender, MouseEventArgs e)
        {
            ButtonBehavior.btnMouseLeave(sender);
        }

        private void itmPOS_Click(object sender, RoutedEventArgs e)
        {
            appContent.Content = posView;
        }

        private void itmTimeClock_Click(object sender, RoutedEventArgs e)
        {
            appContent.Content = clockView;
        }

        private void itmCalendar_Click(object sender, RoutedEventArgs e)
        {
            appContent.Content = new CalendarView(this);
        }

        private void itmSchedule_Click(object sender, RoutedEventArgs e)
        {
            appContent.Content = new DayView(this, DateTime.Now);
        }

        private void itmEmployees_Click(object sender, RoutedEventArgs e)
        {
            appContent.Content = new EmployeesView();
        }

        private void itmProducts_Click(object sender, RoutedEventArgs e)
        {
            appContent.Content = new ProductsView(0);
        }

        private void itmShipments_Click(object sender, RoutedEventArgs e)
        {
            appContent.Content = new ShippingView(this);
        }

        private void itmPayroll_Click(object sender, RoutedEventArgs e)
        {

        }

        private void itmReports_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnPOS_MouseDown(object sender, MouseButtonEventArgs e)
        {
            appContent.Content = posView;
        }
    }
}
