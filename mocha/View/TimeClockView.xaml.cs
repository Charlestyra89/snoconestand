﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using mocha.ViewModel;
using System.ComponentModel;

namespace mocha.View
{
    /// <summary>
    /// Interaction logic for TimeClockView.xaml
    /// </summary>
    public partial class TimeClockView : UserControl
    {
        private TimeClockEventsViewModel vmEvents;
        private long empId;
        private MainWindow owner;
        BackgroundWorker bw = new BackgroundWorker();

        public TimeClockView(MainWindow parent)
        {
            InitializeComponent();
            owner = parent;
            txtblkTime.Text = DateTime.Now.ToString("HH:mm tt");
            txtblkDate.Text = DateTime.Now.ToString("ddd, MMM d yyyy");
            bw.WorkerSupportsCancellation = false;
            bw.WorkerReportsProgress = true;
            bw.DoWork += new DoWorkEventHandler(bw_DoWork);
            bw.ProgressChanged += new ProgressChangedEventHandler
                    (bw_ProgressChanged);
            bw.RunWorkerAsync();
        }

        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            int i = 1;
            while(true)
            {
                if ((worker.CancellationPending == true))
                {
                    e.Cancel = true;
                    break;
                }
                else
                {
                    // Perform a time consuming operation and report progress.
                    System.Threading.Thread.Sleep(1000);
                    worker.ReportProgress((i + 1));
                }
            }
        }

        private void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            this.txtblkTime.Text = DateTime.Now.ToString("HH:mm tt");
            this.txtblkDate.Text = DateTime.Now.ToString("ddd, MMM d yyyy");
        }

        private void btnEnter_MouseDown(object sender, MouseButtonEventArgs e)
        {
            empId = EmployeeViewModel.tryLogin(txtUsername.Text, txtPin.Text);
            if (empId == -1)
            {
                txtError.Text = "Invalid credentials. Try again.";
            }
            else
            {
                txtError.Text = "";
                vmEvents = new TimeClockEventsViewModel(empId);
                owner.appContent.Content = new TimeClockEventView(owner, vmEvents, empId);
            }
            txtUsername.Text = "";
            txtPin.Text = "";
        }

        private void Border_MouseEnter(object sender, MouseEventArgs e)
        {
            ButtonBehavior.btnMouseEnter(sender);
        }

        private void Border_MouseLeave(object sender, MouseEventArgs e)
        {
            ButtonBehavior.btnMouseLeave(sender);
        }
    }
}
