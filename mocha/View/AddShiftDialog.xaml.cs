﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace mocha.View
{
    /// <summary>
    /// Interaction logic for AddShiftDialog.xaml
    /// </summary>
    public partial class AddShiftDialog : Window
    {
        DayView owner;
        public DateTime newStart;
        public DateTime newEnd;
        public bool valid = false;

        public AddShiftDialog(DayView parent)
        {
            InitializeComponent();
            Owner = Application.Current.MainWindow;
            owner = parent;
            txtStart.Tag = DateTime.Now.Date.AddHours(8);
            txtEnd.Tag = DateTime.Now.Date.AddHours(9);
            txtStart.Text = ((DateTime)txtStart.Tag).ToString("hh:mm tt");
            txtEnd.Text = ((DateTime)txtEnd.Tag).ToString("hh:mm tt");
        }

        private void btnEndUp_Click(object sender, RoutedEventArgs e)
        {
            DateTime baseTime = (DateTime)txtEnd.Tag;
            if (baseTime.AddMinutes(30) <= DateTime.Now.Date.AddMinutes(1440))
            {
                baseTime = baseTime.AddMinutes(30);
                txtEnd.Tag = baseTime;
                txtEnd.Text = ((DateTime)txtEnd.Tag).ToString("hh:mm tt");
            }
        }

        private void btnEndDown_Click(object sender, RoutedEventArgs e)
        {
            DateTime baseTime = (DateTime)txtEnd.Tag;
            if (baseTime.AddMinutes(-30) >= DateTime.Now.Date.AddMinutes(30))
            {
                baseTime = baseTime.AddMinutes(-30);
                txtEnd.Tag = baseTime;
                txtEnd.Text = ((DateTime)txtEnd.Tag).ToString("hh:mm tt");
            }
        }

        private void btnStartUp_Click(object sender, RoutedEventArgs e)
        {
            DateTime baseTime = (DateTime)txtStart.Tag;
            if (baseTime.AddMinutes(30) <= DateTime.Now.Date.AddMinutes(1410))
            {
                baseTime = baseTime.AddMinutes(30);
                txtStart.Tag = baseTime;
                txtStart.Text = ((DateTime)txtStart.Tag).ToString("hh:mm tt");
            }
        }

        private void btnStartDown_Click(object sender, RoutedEventArgs e)
        {
            DateTime baseTime = (DateTime)txtStart.Tag;
            if (baseTime.AddMinutes(-30) >= DateTime.Now.Date)
            {
                baseTime = baseTime.AddMinutes(-30);
                txtStart.Tag = baseTime;
                txtStart.Text = ((DateTime)txtStart.Tag).ToString("hh:mm tt");
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DateTime startTime, endTime;
            bool startTimeValid = DateTime.TryParse(txtStart.Text, out startTime);
            bool endTimeValid = DateTime.TryParse(txtEnd.Text, out endTime);
            if (startTimeValid && endTimeValid)
            {
                if (endTime - startTime > new TimeSpan(0, 0, 0))
                {
                    txtError.Text = "";
                    newStart = startTime;
                    newEnd = endTime;
                    valid = true;
                    this.Close();
                }
                else
                {
                    txtError.Text = "Start time must be before end time!";
                    valid = false;
                }
            }
            else
            {
                txtError.Text = "Invalid time format entered!";
                valid = false;
            }
        }
    }
}
