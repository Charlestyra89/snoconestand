﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using mocha.ViewModel;

namespace mocha.View
{
    /// <summary>
    /// Interaction logic for DashboardView.xaml
    /// </summary>
    public partial class DashboardView : UserControl
    {
        private MainWindow owner;
        double left, top;
        public DashboardView(MainWindow parent)
        {
            InitializeComponent();
            System.Drawing.Point clockLoc = Properties.Settings.Default.clockWidgetLoc;
            System.Drawing.Point lunchLoc = Properties.Settings.Default.lunchWidgetLoc;
            if (clockLoc.X != 0 && clockLoc.Y != 0)
            {
                stpOnClock.SetValue(Canvas.LeftProperty, (double)Properties.Settings.Default.clockWidgetLoc.X);
                stpOnClock.SetValue(Canvas.TopProperty, (double)Properties.Settings.Default.clockWidgetLoc.Y);
            }
            if (lunchLoc.X != 0 && lunchLoc.Y != 0)
            {
                stpOnLunch.SetValue(Canvas.LeftProperty, (double)Properties.Settings.Default.lunchWidgetLoc.X);
                stpOnLunch.SetValue(Canvas.TopProperty, (double)Properties.Settings.Default.lunchWidgetLoc.Y);
            }
            owner = parent;
            this.DataContext = owner.vmClock;
        }

       
        private void stpOnClock_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Point p = Mouse.GetPosition(stpOnClock);
            left = p.X;
            top = p.Y;
            stpOnClock.MouseMove += stpOnClock_MouseMove;
            stpOnClock.CaptureMouse();
        }

        void stpOnClock_MouseMove(object sender, MouseEventArgs e)
        {
            Point p = Mouse.GetPosition(canDash);
            
            stpOnClock.SetValue(Canvas.LeftProperty, p.X - left);
            stpOnClock.SetValue(Canvas.TopProperty, p.Y - top);
        }

        private void stpOnClock_MouseUp(object sender, MouseButtonEventArgs e)
        {
            stpOnClock.MouseMove -= stpOnClock_MouseMove;
            stpOnClock.ReleaseMouseCapture();
        }

        private void stpOnLunch_MouseUp(object sender, MouseButtonEventArgs e)
        {
            stpOnLunch.MouseMove -= stpOnLunch_MouseMove;
            stpOnLunch.ReleaseMouseCapture();
        }

        private void stpOnLunch_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Point p = Mouse.GetPosition(stpOnLunch);
            left = p.X;
            top = p.Y;
            stpOnLunch.MouseMove += stpOnLunch_MouseMove;
            stpOnLunch.CaptureMouse();
        }

        void stpOnLunch_MouseMove(object sender, MouseEventArgs e)
        {
            Point p = Mouse.GetPosition(canDash);

            stpOnLunch.SetValue(Canvas.LeftProperty, p.X - left);
            stpOnLunch.SetValue(Canvas.TopProperty, p.Y - top);
        }

        public void saveDashLayout()
        {
            Properties.Settings.Default.clockWidgetLoc = new System.Drawing.Point((int)((double)stpOnClock.GetValue(Canvas.LeftProperty)), (int)((double)stpOnClock.GetValue(Canvas.TopProperty)));
            Properties.Settings.Default.lunchWidgetLoc = new System.Drawing.Point((int)((double)stpOnLunch.GetValue(Canvas.LeftProperty)), (int)((double)stpOnLunch.GetValue(Canvas.TopProperty)));
            Properties.Settings.Default.Save();
        }
    }
}
