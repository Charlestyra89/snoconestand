﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using mocha.ViewModel;

namespace mocha.View
{
    /// <summary>
    /// Interaction logic for AddEmployeeToShift.xaml
    /// </summary>
    public partial class AddEmployeeToShift : Window
    {
        public long selectedId;
        public string emp;
        private EmployeesViewModel vmEmps;

        public AddEmployeeToShift()
        {
            InitializeComponent();
            Owner = Application.Current.MainWindow;
            vmEmps = new EmployeesViewModel();
            this.DataContext = vmEmps;
            cmbEmployees.ItemsSource = vmEmps.employees;
            cmbEmployees.DisplayMemberPath = "fullName";
            cmbEmployees.SelectedValuePath = "id";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            emp = cmbEmployees.Text;
            selectedId = (long)cmbEmployees.SelectedValue;
            this.Close();
        }
    }
}
