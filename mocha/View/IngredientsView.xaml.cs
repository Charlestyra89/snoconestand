﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using mocha.ViewModel;
using mocha.Model;

namespace mocha.View
{
    /// <summary>
    /// Interaction logic for IngredientsView.xaml
    /// </summary>
    public partial class IngredientsView : Window
    {
        IngredientsViewModel vmIngredients;
        RecipeView owner;
        int index;

        public IngredientsView(string name, int _index, RecipeView parent)
        {
            InitializeComponent();
            owner = parent;
            index = _index;
            vmIngredients = new IngredientsViewModel(name);
            this.DataContext = vmIngredients;
        }

        private void dgIngredients_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            addIngredient();
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            addIngredient();
        }

        private void addIngredient()
        {
            owner.vmRecipe.ingredients.Remove((IngredientItem)owner.gridIngredients.Items[index]);
            if (dgIngredients.SelectedItem != null)
            {
                IngredientItem item = (IngredientItem)dgIngredients.SelectedItem;
                owner.vmRecipe.ingredients.Add(item);
            }
            owner.vmRecipe.ingredients.Add(new IngredientItem());
            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
