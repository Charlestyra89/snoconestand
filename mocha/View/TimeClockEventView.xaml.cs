﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using mocha.ViewModel;
using mocha.Model;
using System.ComponentModel;

namespace mocha.View
{
    /// <summary>
    /// Interaction logic for TimeClockEventView.xaml
    /// </summary>
    public partial class TimeClockEventView : UserControl
    {
        public TimeClockEventsViewModel vmEvents;
        MainWindow owner;
        private long empId;
        private EmployeeViewModel vmEmployee;
        BackgroundWorker bw = new BackgroundWorker();

        public TimeClockEventView(MainWindow parent, TimeClockEventsViewModel _events, long empId)
        {
            InitializeComponent();
            owner = parent;
            vmEmployee = EmployeeViewModel.getEmployee(empId);
            txtblkTime.Text = DateTime.Now.ToString("HH:mm tt");
            txtblkDate.Text = DateTime.Now.ToString("ddd, MMM d yyyy");
            bw.WorkerSupportsCancellation = false;
            bw.WorkerReportsProgress = true;
            bw.DoWork += new DoWorkEventHandler(bw_DoWork);
            bw.ProgressChanged += new ProgressChangedEventHandler
                    (bw_ProgressChanged);
            bw.RunWorkerAsync();
            this.vmEvents = _events;
            this.empId = empId;
                    if (vmEmployee.clockStatus == 1 || vmEmployee.clockStatus == 3)
                    {
                        btnLunchOut.Visibility = System.Windows.Visibility.Visible;

                        btnClockIn.Visibility = System.Windows.Visibility.Collapsed;
                        btnClockOut.Visibility = System.Windows.Visibility.Visible;
                        btnLunchIn.Visibility = System.Windows.Visibility.Collapsed;

                    }
                    else if (vmEmployee.clockStatus == 2)
                    {
                        btnLunchOut.Visibility = System.Windows.Visibility.Collapsed;
                        btnClockOut.Visibility = System.Windows.Visibility.Collapsed;

                        btnClockIn.Visibility = System.Windows.Visibility.Collapsed;
                        btnLunchIn.Visibility = System.Windows.Visibility.Visible;

                    }
                    else if (vmEmployee.clockStatus == 0)
                    {
                        btnClockOut.Visibility = System.Windows.Visibility.Collapsed;
                        btnLunchOut.Visibility = System.Windows.Visibility.Collapsed;

                        btnClockIn.Visibility = System.Windows.Visibility.Visible;
                        btnLunchIn.Visibility = System.Windows.Visibility.Collapsed;

                    }
                    else
                    {
                        btnClockIn.Visibility = System.Windows.Visibility.Visible;

                        btnClockOut.Visibility = System.Windows.Visibility.Collapsed;
                        btnLunchOut.Visibility = System.Windows.Visibility.Collapsed;
                        btnLunchIn.Visibility = System.Windows.Visibility.Collapsed;

                    }
          
  
        }

        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            int i = 1;
            while (true)
            {
                if ((worker.CancellationPending == true))
                {
                    e.Cancel = true;
                    break;
                }
                else
                {
                    // Perform a time consuming operation and report progress.
                    System.Threading.Thread.Sleep(1000);
                    worker.ReportProgress((i + 1));
                }
            }
        }

        private void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            this.txtblkTime.Text = DateTime.Now.ToString("HH:mm tt");
            this.txtblkDate.Text = DateTime.Now.ToString("ddd, MMM d yyyy");
        }

        private void btnClockOut_MouseDown(object sender, MouseButtonEventArgs e)
        {
            TimeClockEventsViewModel.insertEvent(4, empId);
            owner.vmClock.ClockOut(vmEmployee);
            owner.appContent.Content = owner.dashView;
        }

        private void btnLunchIn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            TimeClockEventsViewModel.insertEvent(3, empId);
            owner.vmClock.LunchIn(vmEmployee);
            owner.appContent.Content = owner.dashView;
        }

        private void btnLunchOut_MouseDown(object sender, MouseButtonEventArgs e)
        {
            TimeClockEventsViewModel.insertEvent(2, empId);
            owner.vmClock.LunchOut(vmEmployee);
            owner.appContent.Content = owner.dashView;
        }

        private void btnClockIn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            TimeClockEventsViewModel.insertEvent(1, empId);
            owner.vmClock.ClockIn(vmEmployee);
            owner.appContent.Content = owner.dashView;
        }

        private void btnClockIn_MouseEnter(object sender, MouseEventArgs e)
        {
            ButtonBehavior.btnMouseEnter(sender);
        }

        private void btnClockIn_MouseLeave(object sender, MouseEventArgs e)
        {
            ButtonBehavior.btnMouseLeave(sender);
        }
    }
}
