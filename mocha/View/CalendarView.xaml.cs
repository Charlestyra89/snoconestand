﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using mocha.ViewModel;

namespace mocha.View
{
    /// <summary>
    /// Interaction logic for CalendarView.xaml
    /// </summary>
    public partial class CalendarView : UserControl
    {
        int thisMonth;
        int thisDate = -1;
        int thisYear;
        DayOfWeek firstWeekDay;
        int daysInMonth;
        int intFirstWeekDay;
        string nameOfMonth;
        Point scrollMousePoint = new Point();
        double vOff = 1;
        CalendarItemsViewModel vmCalItems;
        MainWindow owner;

        public CalendarView(MainWindow parent)
        {
            InitializeComponent();
            owner = parent;
            thisYear = DateTime.Now.Year;
            thisMonth = DateTime.Now.Month;
            nameOfMonth = getMonthName(thisMonth);
            daysInMonth = DateTime.DaysInMonth(thisYear, thisMonth);
            vmCalItems = new CalendarItemsViewModel(new DateTime(thisYear, thisMonth, 1), new DateTime(thisYear, thisMonth, daysInMonth));
            thisDate = DateTime.Now.Day;
            firstWeekDay = (new DateTime(thisYear, thisMonth, 1)).DayOfWeek;
            lblDate.Text = nameOfMonth + ", " + thisYear.ToString();
            switch (firstWeekDay)
            {
                case DayOfWeek.Sunday:
                    intFirstWeekDay = 0;
                    break;
                case DayOfWeek.Monday:
                    intFirstWeekDay = 1;
                    break;
                case DayOfWeek.Tuesday:
                    intFirstWeekDay = 2;
                    break;
                case DayOfWeek.Wednesday:
                    intFirstWeekDay = 3;
                    break;
                case DayOfWeek.Thursday:
                    intFirstWeekDay = 4;
                    break;
                case DayOfWeek.Friday:
                    intFirstWeekDay = 5;
                    break;
                case DayOfWeek.Saturday:
                    intFirstWeekDay = 6;
                    break;
            }
            generateCalendar();
            addEvents();
        }

        public CalendarView(MainWindow parent, int _month, int _year)
        {
            InitializeComponent();
            owner = parent;
            thisYear = _year;
            thisMonth = _month;
            nameOfMonth = getMonthName(thisMonth);
            daysInMonth = DateTime.DaysInMonth(thisYear, thisMonth);
            vmCalItems = new CalendarItemsViewModel(new DateTime(thisYear, thisMonth, 1), new DateTime(thisYear, thisMonth, daysInMonth));
            firstWeekDay = (new DateTime(thisYear, thisMonth, 1)).DayOfWeek;
            lblDate.Text = nameOfMonth + ", " + thisYear.ToString();
            switch (firstWeekDay)
            {
                case DayOfWeek.Sunday:
                    intFirstWeekDay = 0;
                    break;
                case DayOfWeek.Monday:
                    intFirstWeekDay = 1;
                    break;
                case DayOfWeek.Tuesday:
                    intFirstWeekDay = 2;
                    break;
                case DayOfWeek.Wednesday:
                    intFirstWeekDay = 3;
                    break;
                case DayOfWeek.Thursday:
                    intFirstWeekDay = 4;
                    break;
                case DayOfWeek.Friday:
                    intFirstWeekDay = 5;
                    break;
                case DayOfWeek.Saturday:
                    intFirstWeekDay = 6;
                    break;
            }
            generateCalendar();
            addEvents();
        }

        private void addEvents()
        {
            foreach (CalendarItemViewModel item in vmCalItems)
            {
                Border parent = (from r in grdCalendar.Children.OfType<Border>()
                                    where r.Tag != null && item.startTime >= DateTime.Parse(r.Tag.ToString()) && item.startTime < DateTime.Parse(r.Tag.ToString()).AddDays(1)
                                    select r).ToList<Border>().First();
                var parentGrd = (Grid)parent.Child;
                ScrollViewer scr = (ScrollViewer)parentGrd.Children[1];
                var grd = (Grid)scr.Content;
                RowDefinition newRow = new RowDefinition();
                newRow.Height = new GridLength(30);
                grd.RowDefinitions.Insert(0, newRow);
                var bor = new Border();
                bor.Tag = item.id;
                bor.Background = Brushes.Tomato;
                bor.Margin = new Thickness(3);
                bor.CornerRadius = new CornerRadius(15);
                var txt = new TextBlock();
                txt.Text = item.startTime.ToString("hh:mm tt") + " " + item.title;
                txt.TextWrapping = TextWrapping.Wrap;
                txt.Foreground = Brushes.White;
                txt.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                bor.Child = txt;
                bor.SetValue(Grid.RowProperty, grd.RowDefinitions.Count - 2);
                grd.Children.Add(bor);
            }
        }

        private void generateCalendar()
        {
            Border bor;
            Grid grdCalItem, grdEvents;
            TextBlock txtDate;
            ScrollViewer scrEvents;
            SolidColorBrush transBlack = new SolidColorBrush();
            RowDefinition quarDef;
            transBlack.Color = Colors.Black;
            transBlack.Opacity = 0.7;
            int row = 2;
            int col = intFirstWeekDay;
            for (int i = 1; i <= daysInMonth; i++)
            {
                if (row >= grdCalendar.RowDefinitions.Count)
                    grdCalendar.RowDefinitions.Add(new RowDefinition());
                bor = new Border();
                bor.SetValue(Grid.ColumnProperty, col);
                bor.SetValue(Grid.RowProperty, row);
                if (thisDate == i)
                {
                    bor.BorderBrush = Brushes.Green;
                    bor.BorderThickness = new Thickness(7);
                }
                else
                {
                    bor.BorderBrush = Brushes.Gray;
                    bor.BorderThickness = new Thickness(3);
                }
                bor.CornerRadius = new CornerRadius(12);
                bor.Background = transBlack;
                bor.Tag = new DateTime(thisYear, thisMonth, i);
                quarDef = new RowDefinition();
                quarDef.Height = new GridLength(.25, GridUnitType.Star);
                grdCalItem = new Grid();
                grdCalItem.RowDefinitions.Add(quarDef);
                grdCalItem.RowDefinitions.Add(new RowDefinition());
                grdEvents = new Grid();
                RowDefinition newRow = new RowDefinition();
                newRow.Height = new GridLength(50);
                grdEvents.RowDefinitions.Add(newRow);
                txtDate = new TextBlock();
                txtDate.Text = i.ToString();
                txtDate.Foreground = Brushes.White;
                txtDate.FontSize = 20;
                txtDate.SetValue(Grid.RowProperty, 0);
                scrEvents = new ScrollViewer();
                scrEvents.Tag = new DateTime(thisYear, thisMonth, i);
                scrEvents.VerticalScrollBarVisibility = ScrollBarVisibility.Hidden;
                scrEvents.HorizontalScrollBarVisibility = ScrollBarVisibility.Hidden;
                scrEvents.SetValue(Grid.RowProperty, 1);
                scrEvents.PreviewMouseLeftButtonDown += scrollViewer_PreviewMouseLeftButtonDown;
                scrEvents.PreviewMouseMove += scrollViewer_PreviewMouseMove;
                scrEvents.PreviewMouseLeftButtonUp += scrollViewer_PreviewMouseLeftButtonUp;
                scrEvents.PreviewMouseWheel += scrollViewer_PreviewMouseWheel;
                scrEvents.Content = grdEvents;
                grdCalItem.Children.Add(txtDate);
                grdCalItem.Children.Add(scrEvents);
                bor.Child = grdCalItem;
                grdCalendar.Children.Add(bor);
                col++;
                if (col == 7)
                {
                    col = 0;
                    row++;
                }
            }
        }

        private void scrollViewer_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                ScrollViewer scr = (ScrollViewer)sender;
                Grid grd = (Grid)scr.Content;
                int eventId = -1;
                int row = -1;
                if (grd != null)
                {
                    var pos = e.GetPosition(grd);
                    var temp = pos.Y;
                    for (var i = 0; i < grd.RowDefinitions.Count; i++)
                    {
                        var rowDef = grd.RowDefinitions[i];
                        temp -= rowDef.ActualHeight;
                        if (temp <= -1)
                        {
                            row = i;
                            Border bor = (from r in grd.Children.OfType<Border>()
                                   where (int)r.GetValue(Grid.RowProperty) == row
                                   select r).ToList<Border>().FirstOrDefault();
                            if (bor != null && bor.Tag != null)
                                eventId = (int)bor.Tag;
                            break;
                        }
                    }
                    owner.appContent.Content = new CalendarItemView(eventId, DateTime.Parse(scr.Tag.ToString()), owner.appContent.Content.ToString(), owner);
                }
            }
            else
            {
                ((ScrollViewer)sender).CaptureMouse();
                scrollMousePoint = e.GetPosition((ScrollViewer)sender);
                vOff = ((ScrollViewer)sender).VerticalOffset;
            }
        }

        private void scrollViewer_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (((ScrollViewer)sender).IsMouseCaptured)
            {
                ((ScrollViewer)sender).ScrollToVerticalOffset(vOff + (scrollMousePoint.Y - e.GetPosition(((ScrollViewer)sender)).Y));
            }
        }

        private void scrollViewer_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ((ScrollViewer)sender).ReleaseMouseCapture();
        }

        private void scrollViewer_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            ((ScrollViewer)sender).ScrollToHorizontalOffset(((ScrollViewer)sender).HorizontalOffset + e.Delta);
        }

        private void Border_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ButtonBehavior.btnMouseDown(sender);
        }

        private void btnWeek_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ButtonBehavior.btnMouseDown(sender);
            owner.appContent.Content = new WeekView(owner, DateTime.Now);
        }

        private void btnDay_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ButtonBehavior.btnMouseDown(sender);
            owner.appContent.Content = new DayView(owner, DateTime.Now);
        }

        private void Border_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ButtonBehavior.btnMouseUp(sender);
            Border bor = (Border)sender;
            TextBlock txt = (TextBlock)bor.Child;
            if (txt.Text == "<")
            {
                if (thisMonth == 1)
                {
                    thisMonth = 12;
                    thisYear--;
                }
                else
                    thisMonth--;
                owner.appContent.Content = new CalendarView(owner, thisMonth, thisYear);
            }
            else
            {
                if (thisMonth == 12)
                {
                    thisMonth = 1;
                    thisYear++;
                }
                else
                    thisMonth++;
                owner.appContent.Content = new CalendarView(owner, thisMonth, thisYear);
            }
        }

        private string getMonthName(int _month)
        {
            switch (_month)
            {
                case 1:
                    return "January";
                case 2:
                    return "February";
                case 3:
                    return "March";
                case 4:
                    return "April";
                case 5:
                    return "May";
                case 6:
                    return "June";
                case 7:
                    return "July";
                case 8:
                    return "August";
                case 9:
                    return "September";
                case 10:
                    return "October";
                case 11:
                    return "November";
                case 12:
                    return "December";
                default:
                    return "January";
            }
        }
    }
}
