﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using mocha.ViewModel;
using System.Collections.ObjectModel;

namespace mocha.View
{
    /// <summary>
    /// Interaction logic for ManagerApprovalView.xaml
    /// </summary>
    public partial class ManagerApprovalView : Window
    {
        POSView owner;
        ObservableCollection<EmployeeViewModel> managers;
        ProductViewModel prod = null;

        public ManagerApprovalView(POSView parent, ProductViewModel _prod)
        {
            InitializeComponent();
            owner = parent;
            managers = EmployeesViewModel.getManagers();
            if (_prod != null)
                prod = _prod;
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            if (managers.Count > 0)
            {
                foreach (EmployeeViewModel man in managers)
                {
                    if (man.userName == txtUsername.Text && man.pin == txtPin.Text)
                    {
                        PriceOverrideView overrideView = new PriceOverrideView(owner, prod);
                        this.Close();
                        overrideView.ShowDialog();
                        break;
                    }
                }
            }
            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.Left = ((POSView)owner).owner.Left + (((POSView)owner).owner.Width - this.ActualWidth) / 2;
            this.Top = ((POSView)owner).owner.Top + (((POSView)owner).owner.Height - this.ActualHeight) / 2;

        }
    }
}
