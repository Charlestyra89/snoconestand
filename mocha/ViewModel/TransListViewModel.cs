﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mocha.Model;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace mocha.ViewModel
{
    public class TransListViewModel
    {
        public ObservableCollection<TransactionViewModel> transactions { get; set; }

        /*
       * retrieve list of transactions for reporting
       */
        public TransListViewModel(bool completed, bool onHold, bool noSales, bool all)
        {
            transactions = new ObservableCollection<TransactionViewModel>();
            POSTransactions data = POSTransactions.getTransactions(completed, onHold, noSales, all);
            TransactionViewModel vmTrans;
            foreach (POSTransaction trans in data)
            {
                vmTrans = new TransactionViewModel();
                vmTrans.id = trans.id;
                vmTrans.initials = trans.initials;
                vmTrans.grandTotal = trans.grandtotal;
                vmTrans.dateEntered = trans.datetime;
                vmTrans.notes = trans.notes;
                transactions.Add(vmTrans);
            }
        }


    }
}
