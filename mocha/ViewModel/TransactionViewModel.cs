﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using mocha.Model;


namespace mocha.ViewModel
{
    public class TransactionViewModel : INotifyPropertyChanged
    {
        private long _id = -1;
        private long _empId = -1;
        private string _initials;
        private double _subtotal = 0;
        private double _tax;
        private double _grandtotal = 0.0;
        private double _amountpaid = 0.0;
        private double _amountdue = 0.0;
        private int _transType;
        private bool _isOnHold;
        private bool _isNoSale;
        private DateTime _dateEntered = DateTime.Now;
        private string _notes;
        private ObservableCollection<ProductViewModel> _products;

        public ObservableCollection<ProductViewModel> products
        {
            get { return _products; }
            set
            {
                _products = value;
                OnPropertyChanged("products");
            }
        }
        public long id
        {
            get { return _id; }
            set
            {
                _id = value;
                OnPropertyChanged("id");
            }
        }
        public long empId
        {
            get { return _empId; }
            set
            {
                _id = value;
                OnPropertyChanged("empId");
            }
        }
        public string initials
        {
            get { return _initials; }
            set
            {
                _initials = value;
                OnPropertyChanged("initials");
            }
        }
        public double subtotal
        {
            get { return _subtotal; }
            set
            {
                _subtotal = value;
                OnPropertyChanged("subtotal");
                this.tax = subtotal * 0.08;
            }
        }
        public double tax
        {
            get { return _tax; }
            set
            {
                _tax = value;
                OnPropertyChanged("tax");
                grandTotal = subtotal + tax;
            }
        }
        public double grandTotal
        {
            get { return _grandtotal; }
            set
            {
                _grandtotal = value;
                OnPropertyChanged("grandtotal");
            }
        }
        public double amountPaid
        {
            get { return _amountpaid; }
            set
            {
                _amountpaid = value;
                OnPropertyChanged("amountPaid");
                amountDue = grandTotal - amountPaid;
            }
        }
        public double amountDue
        {
            get { return _amountdue; }
            set
            {
                _amountdue = value;
                OnPropertyChanged("amountDue");
            }
        }
        public int transType
        {
            get { return _transType; }
            set
            {
                _transType = value;
                OnPropertyChanged("transType");
            }
        }

        public bool isOnHold
        {
            get { return _isOnHold; }
            set
            {
                _isOnHold = value;
                OnPropertyChanged("isOnHold");
            }
        }

        public bool isNoSale
        {
            get { return _isNoSale; }
            set
            {
                _isNoSale = value;
                OnPropertyChanged("isNoSale");
            }
        }
        public DateTime dateEntered
        {
            get { return _dateEntered; }
            set
            {
                _dateEntered = value;
                OnPropertyChanged("dateEntered");
            }
        }
        public string notes
        {
            get { return _notes; }
            set
            {
                _notes = value;
                OnPropertyChanged("notes");
            }
        }
        /*
       * set up a new transaction with a blank list of products
       */
        public TransactionViewModel()
        {
            products = new ObservableCollection<ProductViewModel>();

        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        internal void tryAdd(ProductViewModel item)
        {
            bool contains = false;
            foreach (ProductViewModel vmProd in products)
            {
                if (vmProd.id == item.id)
                {
                    vmProd.qty++;
                    vmProd.modPrice += vmProd.price;
                    contains = true;
                }
            }
            if (!contains)
                this.products.Add(item);
            subtotal += item.price;
        }

        /*
       * retrieve an existing transaction
       */
        internal TransactionViewModel getTransaction(long id)
        {
            POSTransaction data = new POSTransaction();
            ProductViewModel vmProd;
            data.getTransaction(id);
            this.id = data.id;
            this.subtotal = data.subtotal;
            this.tax = data.tax;
            this.grandTotal = data.grandtotal;
            this.amountPaid = data.tendered;
            this.initials = data.initials;
            foreach (ProductItem item in data.products)
            {
                vmProd = new ProductViewModel(this, item.id, item.name, item.price, item.image);
                this.products.Add(vmProd);
            }
            return this;
        }

        /*
       * send new transaction to DB layer
       */
        internal bool commitTrans(bool onHold, bool noSale)
        {
            POSTransaction newTrans = new POSTransaction();
            newTrans.employeeId = Globals.empId;
            newTrans.initials = Globals.initials;
            newTrans.subtotal = subtotal;
            newTrans.tax = tax;
            newTrans.grandtotal = grandTotal;
            newTrans.tendered = amountPaid;
            newTrans.tenderType = transType;
            newTrans.datetime = dateEntered;
            newTrans.notes = notes;
            newTrans.products = new List<ProductItem>();
            foreach (ProductViewModel item in products)
            {
                newTrans.products.Add(new ProductItem(item.id, item.qty));
            }
            try
            {
                newTrans.commitTransaction(onHold, noSale);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
