﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using mocha.Model;

namespace mocha.ViewModel
{
    public class RecipeViewModel
    {
        private ObservableCollection<RecipeStep> _steps;
        private ObservableCollection<IngredientItem> _ingredients;

        public ObservableCollection<RecipeStep> steps 
        {
            get { return _steps; }
            set
            {
                _steps = value;
            }
        }

        public ObservableCollection<IngredientItem> ingredients
        {
            get { return _ingredients; }
            set
            {
                _ingredients = value;
            }
        }
        public RecipeViewModel()
        {
            steps = new ObservableCollection<RecipeStep>();
            ingredients = new ObservableCollection<IngredientItem>();
        }

        /*
       * retrieve list of ingredients and recipe steps
       */
        public RecipeViewModel(long id)
        {
            RecipeSteps rawRec = RecipeSteps.getRecipeSteps(id);
            Ingredients rawIngr = Ingredients.getIngredients(id);
            steps = new ObservableCollection<RecipeStep>();
            ingredients = new ObservableCollection<IngredientItem>();
            foreach (RecipeStep step in rawRec)
            {
                steps.Add(step);
            }
            foreach (IngredientItem item in rawIngr)
            {
                ingredients.Add(item);
            }
        }

        /*
       * send data to DB layer
       */
        public bool updateRecipe(long id)
        {
            RecipeSteps rawData = new RecipeSteps();
            bool result = true;
            if (steps != null)
            {
                int index = 1;
                for (int i = 0; i <= steps.Count - 1; i++)
                {
                    steps[i].sort = index++;
                    rawData.Add(steps[i]);
                }
                result = rawData.updateRecipe(id);
            }
            return result;
        }

        /*
       * send data to DB layer
       */
        public bool updateIngredients(long id)
        {
            Ingredients ingr = new Ingredients();
            bool result = true;
            if (ingredients != null)
            {
                int index = 1;
                for (int i = 0; i <= ingredients.Count - 1; i++)
                {
                    if (ingredients[i].id != 0)
                    {
                        ingredients[i].sort = index++;
                        ingr.Add(ingredients[i]);
                    }
                }
                result = ingr.updateIngredients(id);
            }
            return result;
        }
    }
}
