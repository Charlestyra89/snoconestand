﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;

namespace mocha.ViewModel
{
    static class ButtonBehavior
    {
        /*
         * generic button behavior events for entire application 
         */
        public static void btnMouseEnter(object sender)
        {
            Border bor = (Border)sender;
            TextBlock block = (TextBlock)bor.Child;
            bor.Background = Brushes.White;
            block.Foreground = Brushes.Black;
        }

        public static void btnMouseLeave(object sender)
        {
            Border bor = (Border)sender;
            TextBlock block = (TextBlock)bor.Child;
            bor.Background = Brushes.Transparent;
            block.Foreground = Brushes.White;
        }

        public static void btnMouseDown(object sender)
        {
            Border bor = (Border)sender;
            TextBlock block = (TextBlock)bor.Child;
            bor.Background = Brushes.White;
            block.Foreground = Brushes.Black;
        }

        public static void btnMouseUp(object sender)
        {
            Border bor = (Border)sender;
            TextBlock block = (TextBlock)bor.Child;
            bor.Background = Brushes.Transparent;
            block.Foreground = Brushes.White;
        }
    }
}
