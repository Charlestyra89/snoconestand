﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using mocha.Model;

namespace mocha.ViewModel
{
    public class IngredientsViewModel : INotifyPropertyChanged
    {
        public ObservableCollection<IngredientItem> ingredients { get; set; }

        public IngredientsViewModel()
        {
            ingredients = new ObservableCollection<IngredientItem>();
        }

        /*
       * find ingredients with matching name
       */
        public IngredientsViewModel(string name)
        {
            ingredients = new ObservableCollection<IngredientItem>();
            Ingredients data = Ingredients.searchIngredients(name);
            foreach (IngredientItem item in data)
            {
                ingredients.Add(item);
            }
        }

        /*
       * retrieve list of ingredients for designated product
       */
        public IngredientsViewModel(long productId)
        {
            ingredients = new ObservableCollection<IngredientItem>();
            Ingredients data = Ingredients.getIngredients(productId);
            foreach (IngredientItem item in data)
            {
                ingredients.Add(item);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }
}
