﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using mocha.Model;

namespace mocha.ViewModel
{
    public class SuppliersViewModel : ObservableCollection<Supplier>
    {
        /*
       * retrieve list of suppliers
       */
        public SuppliersViewModel()
        {
            Suppliers data = Suppliers.getSuppliers();
            foreach (Supplier sup in data)
            {
                this.Add(sup);
            }
        }
    }
}
