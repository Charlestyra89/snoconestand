﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using mocha.Model;

namespace mocha.ViewModel
{
    public class TimeClockViewModel : INotifyPropertyChanged
    {
        public ObservableCollection<EmployeeViewModel> employeesOnClock { get; set; }
        public ObservableCollection<EmployeeViewModel> employeesOnLunch { get; set; }

        /*
       * retrieve list of employees who are clocked in and/or on break
       */
        public TimeClockViewModel()
        {
            employeesOnClock = new ObservableCollection<EmployeeViewModel>();
            employeesOnLunch = new ObservableCollection<EmployeeViewModel>();
            EmployeesViewModel vmEmps = new EmployeesViewModel();
            foreach (EmployeeViewModel emp in vmEmps.employees)
                {
                    if(emp.clockStatus == 1 || emp.clockStatus == 5)
                        employeesOnClock.Add(emp);
                    else if (emp.clockStatus == 3)
                        employeesOnLunch.Add(emp);
                }
        }

        public void ClockIn(EmployeeViewModel _emp)
        {
            employeesOnClock.Add(_emp);
        }

        public void ClockOut(EmployeeViewModel _emp)
        {
            foreach (EmployeeViewModel vmEmp in employeesOnClock)
            {
                if (vmEmp.id == _emp.id)
                {
                    employeesOnClock.Remove(vmEmp);
                    break;
                }
            }
        }

        public void LunchIn(EmployeeViewModel _emp)
        {
            employeesOnClock.Add(_emp);
            foreach (EmployeeViewModel vmEmp in employeesOnLunch)
            {
                if (vmEmp.id == _emp.id)
                {
                    employeesOnLunch.Remove(vmEmp);
                    break;
                }
            }
            
        }

        public void LunchOut(EmployeeViewModel _emp)
        {
            foreach (EmployeeViewModel vmEmp in employeesOnClock)
            {
                if (vmEmp.id == _emp.id)
                {
                    employeesOnClock.Remove(vmEmp);
                    break;
                }
            }
            employeesOnLunch.Add(_emp);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
