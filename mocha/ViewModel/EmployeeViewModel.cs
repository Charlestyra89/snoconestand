﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using mocha.Model;

namespace mocha.ViewModel
{
    public class EmployeeViewModel : INotifyPropertyChanged
    {
        private long _id;
        private string _lastName;
        private string _firstName;
        private string _phone;
        private string _email;
        private string _addressLine1;
        private string _addressLine2;
        private string _city;
        private string _state;
        private string _zip;
        private float _payRate;
        private string _userName;
        private string _pin;
        private bool _isManager;
        private int _clockStatus;

        public long id
        {
            get { return _id; }
            set
            {
                _id = value;
                OnPropertyChanged("id");
            }
        }
        public string fullName
        {
            get { return _firstName + " " + _lastName; }
        }
        public string lastName
        {
            get { return _lastName; }
            set
            {
                _lastName = value;
                OnPropertyChanged("lastName");
            }
        }
        public string firstName
        {
            get { return _firstName; }
            set
            {
                _firstName = value;
                OnPropertyChanged("firstName");
            }
        }
        public string phone
        {
            get { return _phone; }
            set
            {
                _phone = value;
                OnPropertyChanged("phone");
            }
        }
        public string email
        {
            get { return _email; }
            set
            {
                _email = value;
                OnPropertyChanged("email");
            }
        }
        public string addressLine1
        {
            get { return _addressLine1; }
            set
            {
                _addressLine1 = value;
                OnPropertyChanged("addressLine1");
            }
        }
        public string addressLine2
        {
            get { return _addressLine2; }
            set
            {
                _addressLine2 = value;
                OnPropertyChanged("addressLine2");
            }
        }
        public string city
        {
            get { return _city; }
            set
            {
                _city = value;
                OnPropertyChanged("city");
            }
        }
        public string state
        {
            get { return _state; }
            set
            {
                _state = value;
                OnPropertyChanged("state");
            }
        }
        public string zip
        {
            get { return _zip; }
            set
            {
                _zip = value;
                OnPropertyChanged("zip");
            }
        }
        public float payRate
        {
            get { return _payRate; }
            set
            {
                _payRate = value;
                OnPropertyChanged("payRate");
            }
        }
        public string userName
        {
            get { return _userName; }
            set
            {
                _userName = value;
                OnPropertyChanged("userName");
            }
        }
        public string pin
        {
            get { return _pin; }
            set
            {
                _pin = value;
                OnPropertyChanged("pin");
            }
        }
        public bool isManager
        {
            get { return _isManager; }
            set
            {
                _isManager = value;
                OnPropertyChanged("isManager");
            }
        }
        public int clockStatus
        {
            get { return _clockStatus; }
            set
            {
                _clockStatus = value;
                OnPropertyChanged("clockStatus");
            }
        }

        public EmployeeViewModel()
        {
            id = 0;
        }

        public EmployeeViewModel(long empId)
        {
            Employee emp = Employee.getEmployee(empId);
            this.id = empId;
            this.lastName = emp.lastName;
            this.firstName = emp.firstName;
            this.phone = emp.phone;
            this.email = emp.email;
            this.addressLine1 = emp.addressLine1;
            this.addressLine2 = emp.addressLine2;
            this.city = emp.city;
            this.state = emp.state;
            this.zip = emp.zip;
            this.userName = emp.userName;
            this.pin = emp.pin;
            this.payRate = emp.payRate;
            this.isManager = emp.isManager;
        }

        /*
       * send data to DB layer
       */
        public string commitChanges()
        {
            string result = "";
            try
            {
                Employee emp = new Employee();
                emp.id = this.id;
                emp.lastName = this.lastName;
                emp.firstName = this.firstName;
                emp.phone = this.phone;
                emp.email = this.email;
                emp.addressLine1 = this.addressLine1;
                emp.addressLine2 = this.addressLine2;
                emp.city = this.city;
                emp.state = this.state;
                emp.zip = this.zip;
                emp.userName = this.userName;
                emp.pin = this.pin;
                emp.payRate = this.payRate;
                emp.isManager = this.isManager;
                Employee.commitEmployee(emp);
            }
            catch (Exception ex)
            {
                result = "Unable to commit changes.\n" + ex.Message;
            }
            return result;
        }

        public static EmployeeViewModel getEmployee(long id)
        {
            Employee emp = Employee.getEmployee(id);
            EmployeeViewModel vmEmp = new EmployeeViewModel();
            if (emp == null)
                return null;
            vmEmp.id = emp.id;
            vmEmp.lastName = emp.lastName;
            vmEmp.firstName = emp.firstName;
            vmEmp.addressLine1 = emp.addressLine1;
            vmEmp.addressLine2 = emp.addressLine2;
            vmEmp.city = emp.city;
            vmEmp.email = emp.email;
            vmEmp.isManager = emp.isManager;
            vmEmp.payRate = emp.payRate;
            vmEmp.phone = emp.phone;
            vmEmp.pin = emp.pin;
            vmEmp.state = emp.state;
            vmEmp.userName = emp.userName;
            vmEmp.zip = emp.zip;
            vmEmp.clockStatus = emp.clockStatus;
            return vmEmp;
        }

        public string deleteEmployee()
        {
            string result = "";
            try
            {
                Employee.deleteEmployee(this.id);
            }
            catch (Exception ex)
            {
                result = "Unable to delete employee.\n" + ex.Message;
            }
            return result;
        }

        public static long tryLogin(string userName, string pin)
        {
            return Employee.tryLogin(userName, pin);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        internal void updateClockStatus(int typeId, long empId)
        {
            Employee emp = Employee.getEmployee(empId);
            this.clockStatus = Employee.updateClockStatus(typeId, emp);
        }
    }

    public class EmployeesViewModel : INotifyPropertyChanged
    {
        public ObservableCollection<EmployeeViewModel> employees { get; set; }

        /*
       * populate list of employees
       */
        public EmployeesViewModel()
        {
            employees = new ObservableCollection<EmployeeViewModel>();
            Employees emps = Employees.getEmployees();
            EmployeeViewModel vmEmp;
            foreach (Employee emp in emps)
            {
                vmEmp = new EmployeeViewModel();
                vmEmp.id = emp.id;
                vmEmp.lastName = emp.lastName;
                vmEmp.firstName = emp.firstName;
                vmEmp.addressLine1 = emp.addressLine1;
                vmEmp.addressLine2 = emp.addressLine2;
                vmEmp.city = emp.city;
                vmEmp.email = emp.email;
                vmEmp.isManager = emp.isManager;
                vmEmp.payRate = emp.payRate;
                vmEmp.phone = emp.phone;
                vmEmp.pin = emp.pin;
                vmEmp.state = emp.state;
                vmEmp.userName = emp.userName;
                vmEmp.zip = emp.zip;
                vmEmp.clockStatus = emp.clockStatus;
                employees.Add(vmEmp);
            }
        }

        /*
       * retrieve list of employees where manager is set to true
       */
        public static ObservableCollection<EmployeeViewModel> getManagers()
        {
            EmployeesViewModel vmEmps = new EmployeesViewModel();
            ObservableCollection<EmployeeViewModel> managers = new ObservableCollection<EmployeeViewModel>();
            foreach (EmployeeViewModel emp in vmEmps.employees)
            {
                if (emp.isManager)
                {
                    managers.Add(emp);
                }
            }
            return managers;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, e);
            }
        }
    }
}
