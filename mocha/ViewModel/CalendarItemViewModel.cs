﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using mocha.Model;

namespace mocha.ViewModel
{
    public class CalendarItemViewModel : INotifyPropertyChanged
    {
        private int _id;
        private string _title;
        private string _description;
        private DateTime _startTime;
        private DateTime _endTime;
        private int _serviceId;
        private bool _paymentRec;

        public int id 
        {
            get { return _id; }
            set
            {
                _id = value;
                OnPropertyChanged("id");
            }
        }
        public string title
        {
            get { return _title; }
            set
            {
                _title = value;
                OnPropertyChanged("title");
            }
        }
        public string description
        {
            get { return _description; }
            set
            {
                _description = value;
                OnPropertyChanged("description");
            }
        }
        public DateTime startTime
        {
            get { return _startTime; }
            set
            {
                _startTime = value;
                OnPropertyChanged("startTime");
            }
        }
        public DateTime endTime
        {
            get { return _endTime; }
            set
            {
                _endTime = value;
                OnPropertyChanged("endTime");
            }
        }
        public int serviceId
        {
            get { return _serviceId; }
            set
            {
                _serviceId = value;
                OnPropertyChanged("serviceId");
            }
        }
        public bool paymentReceived
        {
            get { return _paymentRec; }
            set
            {
                _paymentRec = value;
                OnPropertyChanged("paymentReceived");
            }
        }

        public CalendarItemViewModel()
        {

        }

        public CalendarItemViewModel(int _id, DateTime _date)
        {
            if (_id <= 0)
            {
                startTime = _date;
                endTime = _date;
            }
            else
            {
                CalendarItem item = new CalendarItem(_id);
                id = item.id;
                title = item.title;
                description = item.description;
                startTime = item.startTime;
                endTime = item.endTime;
                serviceId = item.serviceId;
                paymentReceived = item.paymentReceived;
            }
        }

        /*
        * send data to DB layer for saving 
        */
        public bool commitChanges()
        {
            CalendarItem item = new CalendarItem()
            {
                id = this.id,
                title = this.title,
                description = this.description,
                startTime = this.startTime,
                endTime = this.endTime,
                serviceId = this.serviceId,
                paymentReceived = this.paymentReceived
            };
            return item.commit(id);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class CalendarItemsViewModel : ObservableCollection<CalendarItemViewModel>
    {
        /*
        * retrieve all calendar events in the database
        */
        public CalendarItemsViewModel()
        {
            CalendarItems items = new CalendarItems();
            foreach (CalendarItem item in items)
            {
                var vmItem = new CalendarItemViewModel()
                {
                    id = item.id,
                    title = item.title,
                    description = item.description,
                    startTime = item.startTime,
                    endTime = item.endTime,
                    serviceId = item.serviceId,
                    paymentReceived = item.paymentReceived
                };
                this.Add(vmItem);
            }
        }

        /*
        * retreive list of calendar events based on date criteria
        */
        public CalendarItemsViewModel(DateTime _startDay, DateTime _endDay)
        {
            CalendarItems items = new CalendarItems(_startDay, _endDay);
            foreach (CalendarItem item in items)
            {
                var vmItem = new CalendarItemViewModel()
                {
                    id = item.id,
                    title = item.title,
                    description = item.description,
                    startTime = item.startTime,
                    endTime = item.endTime,
                    serviceId = item.serviceId,
                    paymentReceived = item.paymentReceived
                };
                this.Add(vmItem);
            }
        }
    }
}
