﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mocha.Model;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace mocha.ViewModel
{
    public class LotViewModel
    {
        public ObservableCollection<ProductLot> lots { get; set; }

        public LotViewModel()
        {
            lots = new ObservableCollection<ProductLot>();
        }

        /*
       * populate list of product lots
       */
        public LotViewModel(long productId)
        {
            ProductLots data = ProductLots.getLots(productId);
            lots = new ObservableCollection<ProductLot>();
            foreach (ProductLot lot in data)
            {
                lots.Add(lot);
            }
        }

        /*
       * send data to DB layer
       */
        public void commitChanges()
        {
            ProductLots data = new ProductLots();
            if (lots != null)
            {
                foreach (ProductLot lot in lots)
                {
                    data.Add(lot);
                }
                data.commitChanges();
            }
        }
    }
}
