﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using mocha.Model;

namespace mocha.ViewModel
{
    public class MenuGridViewModel
    {
        public ObservableCollection<MenuGridItem> gridItems { get; set; }

        /*
       * retrieve list of categorie
       */
        public MenuGridViewModel()
        {
            MenuGridItems items = MenuGridItems.getMenuGridItems();
            gridItems = new ObservableCollection<MenuGridItem>();
            foreach (MenuGridItem item in items)
            {
                gridItems.Add(item);
            }
        }

        /*
       * send data to DB layer
       */
        public bool commitItems()
        {
            MenuGridItems items = new MenuGridItems();
            foreach (MenuGridItem item in gridItems)
            {
                items.Add(item);
            }
            return items.commitMenuGridItems();
        }
    }
}
