﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.ComponentModel;
using mocha.Model;

namespace mocha.ViewModel
{
    public class ProductViewModel : INotifyPropertyChanged
    {
        private long _id;
        private string _name;
        private double _price;
        private double _modPrice;
        private long _ingredientListId;
        private ObservableCollection<IngredientItem> _ingredients;
        private int _qty = 1;
        private double _qtyPrice;
        private string _qtyDisp = "x1";
        private string _description;
        private bool _hasRecipe = false;
        private bool _isIngredient = false;
        private RecipeViewModel _recipe;
        private DateTime _dateAdded;
        private int _categoryId;
        private TransactionViewModel owner;
        private string _image;
        private bool _isForSale;
        private bool _isSelected;

        public long id
        {
            get { return _id; }
            set
            {
                _id = value;
                OnPropertyChanged("id");
            }
        }
        public string name
        {
            get { return _name; }
            set
            {
                _name = value;
                OnPropertyChanged("name");
            }
        }
        public string qtyDisp
        {
            get { return _qtyDisp; }
            set
            {
                _qtyDisp = value;
                OnPropertyChanged("qtyDisp");
            }
        }
        public double price
        {
            get { return _price; }
            set
            {
                _price = value;
                OnPropertyChanged("price");
            }
        }
        public double modPrice
        {
            get { return _modPrice; }
            set
            {
                _modPrice = value;
                OnPropertyChanged("modPrice");
            }
        }
        public long ingredientListId
        {
            get { return _ingredientListId; }
            set
            {
                _ingredientListId = value;
                OnPropertyChanged("ingredientListId");
            }
        }
        public ObservableCollection<IngredientItem> ingredients
        {
            get { return _ingredients; }
            set
            {
                _ingredients = value;
                OnPropertyChanged("ingredients");
            }
        }
        public int qty
        {
            get { return _qty; }
            set
            {
                _qty = value;
                qtyPrice = qty * price;
                qtyDisp = "x" + qty;
                OnPropertyChanged("qty");
            }
        }
        public string description
        {
            get { return _description; }
            set
            {
                _description = value;
                OnPropertyChanged("description");
            }
        }

        public bool hasRecipe
        {
            get { return _hasRecipe; }
            set
            {
                _hasRecipe = value;
                OnPropertyChanged("hasRecipe");
            }
        }

        public bool isIngredient
        {
            get { return _isIngredient; }
            set
            {
                _isIngredient = value;
                OnPropertyChanged("isIngredient");
            }
        }

        public RecipeViewModel recipe
        {
            get { return _recipe; }
            set
            {
                _recipe = value;
                OnPropertyChanged("recipe");
            }
        }

        public DateTime dateAdded
        {
            get { return _dateAdded; }
            set
            {
                _dateAdded = value;
                OnPropertyChanged("dateAdded");
            }
        }

        public int categoryId
        {
            get { return _categoryId; }
            set
            {
                _categoryId = value;
                OnPropertyChanged("categoryId");
            }
        }

        public string image
        {
            get { return _image; }
            set
            {
                _image = value;
                OnPropertyChanged("image");
            }
        }

        public bool isForSale
        {
            get { return _isForSale; }
            set
            {
                _isForSale = value;
                OnPropertyChanged("isForSale");
            }
        }

        public bool isSelected
        {
            get { return _isSelected; }
            set
            {
                _isSelected = value;
                OnPropertyChanged("isSelected");
            }
        }

        public double qtyPrice
        {
            get { return _qtyPrice; }
            set
            {
                _qtyPrice = value;
                OnPropertyChanged("qtyPrice");
            }
        }

        public ProductViewModel()
        { }

        public ProductViewModel(TransactionViewModel parent)
        {
            owner = parent;
        }

        /*
       * get product interface
       */
        public ProductViewModel(TransactionViewModel parent, long _id, string _name, double _price, string _image)
        {
            owner = parent;
            id = _id;
            name = _name;
            price = _price;
            qty = 1;
            qtyPrice = price * qty;
            image = _image;
        }

        public ProductViewModel(TransactionViewModel parent, long id)
        {
            owner = parent;
            ProductItem prod = ProductItem.getProduct(id);
            if (prod != null)
            {
                this.id = prod.id;
                this.name = prod.name;
                this.price = prod.price;
                this.qty = 1;
                qtyPrice = price * qty;
                this.image = prod.image;
            }
        }

        public ProductViewModel(long prodId)
        {
            ProductItem item = ProductItem.getProduct(prodId);
            if (item != null)
            {
                this.id = item.id;
                this.name = item.name;
                this.price = item.price;
                //this.ingredients new IngredientsViewModel(this.id);
                this.description = item.description;
                this.recipe = new RecipeViewModel();
                this.isIngredient = item.isIngredient;
                this.isForSale = item.isForSale;
                this.hasRecipe = item.hasRecipe;
                this.dateAdded = item.dateDisplayed;
                this.categoryId = item.categoryId;
                this.qty = 1;
            }

        }

        /*
       * send updated product info to DB layer
       */
        public long updateProduct()
        {
            ProductItem item = new ProductItem();
            item.id = this.id;
            item.name = this.name;
            item.categoryId = this.categoryId;
            item.description = this.description;
            item.price = this.price;
            item.isIngredient = this.isIngredient;
            item.hasRecipe = this.hasRecipe;
            bool execUpdate = item.updateProduct();
            if (execUpdate)
                return item.id;
            else
                return 0;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class ProductsViewModel
    {
        public ObservableCollection<ProductViewModel> products { get; set; }

        /*
       * retrieve list of products
       */
        public ProductsViewModel()
        {
            products = new ObservableCollection<ProductViewModel>();
            Products prods = Products.getProducts(0);
            foreach (ProductItem item in prods)
            {
                var vmItem = new ProductViewModel()
                {
                    id = item.id,
                    name = item.name,
                    price = item.price,
                    description = item.description,
                    recipe = new RecipeViewModel(),
                    isIngredient = item.isIngredient,
                    isForSale = item.isForSale,
                    hasRecipe = item.hasRecipe,
                    dateAdded = item.dateDisplayed,
                    categoryId = item.categoryId
                };
                products.Add(vmItem);
            }
        }

        /*
       * retrieve list of products for a specified category
       */
        public ProductsViewModel(int categoryId)
        {
            var prods = getData(categoryId);
            products = new ObservableCollection<ProductViewModel>();
            foreach (ProductItem item in prods)
            {
                var vmItem = new ProductViewModel()
                {
                    id = item.id,
                    name = item.name,
                    price = item.price,
                    description = item.description,
                    recipe = new RecipeViewModel(),
                    isIngredient = item.isIngredient,
                    isForSale = item.isForSale,
                    hasRecipe = item.hasRecipe,
                    dateAdded = item.dateDisplayed,
                    categoryId = item.categoryId
                };
                products.Add(vmItem);
            }
        }

        private ObservableCollection<ProductItem> getData(int categoryId)
        {
            Products rawData = Products.getProducts(categoryId);
            ObservableCollection<ProductItem> products = new ObservableCollection<ProductItem>();
            foreach (ProductItem item in rawData)
            {
                products.Add(item);
            }
            return products;
        }

        /*
       * signal DB layer to soft delete specified product
       */
        public string deleteProduct(long selectedId)
        {
            string result = ProductItem.deleteProduct(selectedId);
            return result;
        }
    }
}
