﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using mocha.Model;

namespace mocha.ViewModel
{
    public class TimeClockEventsViewModel
    {
        public ObservableCollection<TimeClockEvent> events { get; set; }

        /*
       * retrieve list of time clock events based on employee id
       */
        public TimeClockEventsViewModel(long empId)
        {
            events = new ObservableCollection<TimeClockEvent>();
            TimeClockEvents data = TimeClockEvents.getEvents(empId);
            foreach (TimeClockEvent item in data)
            {
                events.Add(item);
            }
        }

        /*
       * signal DB layer to add a new time clock event
       */
        public static void insertEvent(int typeId, long empId)
        {
            TimeClockEvent.insertEvent(typeId, empId);
            EmployeeViewModel emp = EmployeeViewModel.getEmployee(empId);
            emp.updateClockStatus(typeId, empId);
        }
    }
}
