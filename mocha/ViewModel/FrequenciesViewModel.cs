﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mocha.Model;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace mocha.ViewModel
{
    class FrequenciesViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<Frequency> _frequencies;
        public ObservableCollection<Frequency> frequencies 
        {
            get { return _frequencies; }
            set
            {
                _frequencies = value;
                OnPropertyChanged("frequencies");
            }
        }
        /*
       * populate list of frequencies
       */
        public FrequenciesViewModel()
        {
            var freq = new Frequencies();
            frequencies = new ObservableCollection<Frequency>();
            foreach (Frequency item in freq)
            {
                frequencies.Add(item);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
