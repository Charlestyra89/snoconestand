﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mocha.Model;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace mocha.ViewModel
{
    public class ShipmentViewModel
    {
        private double _totalCost = 0;
        private int _totalCases = 0;
        private int _totalPeices = 0;

        public int id { get; set; }
        public DateTime dateReceived { get; set; }
        public string supplier { get; set; }
        public string verifiedBy { get; set; }
        public double totalCost 
        {
            get { return _totalCost; }
            set
            {
                _totalCost = value;
            }
        }
        public int totalCases
        {
            get { return _totalCases; }
            set
            {
                _totalCases = value;
            }
        }
        public int totalPeices
        {
            get { return _totalPeices; }
            set
            {
                _totalPeices = value;
            }
        }
        public string notes { get; set; }

        public ShipmentViewModel()
        {

        }
    }

    public class ShippingViewModel
    {
        public ObservableCollection<ShipmentViewModel> shipments;
        public ProductsViewModel vmProducts;

        /*
       * retrieve list of shipments
       */
        public ShippingViewModel()
        {
            shipments = new ObservableCollection<ShipmentViewModel>();
            vmProducts = new ProductsViewModel();

            Shipments ships = new Shipments();
            ShipmentViewModel vmShip;
            foreach (Shipment ship in ships)
            {
                vmShip = new ShipmentViewModel();
                vmShip.id = ship.shipmentId;
                vmShip.dateReceived = DateTime.FromOADate(ship.dateReceived);
                vmShip.supplier = ship.supplierName;
                vmShip.verifiedBy = ship.verifiedInit;
                foreach (ShipmentItem item in ship.items)
                {
                    vmShip.totalCases += item.cases;
                    vmShip.totalPeices += item.peices;
                    vmShip.totalCost += (item.costPerCase * vmShip.totalCases);
                }
                shipments.Add(vmShip);
            }

        }
    }
}
