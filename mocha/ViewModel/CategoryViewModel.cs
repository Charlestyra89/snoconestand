﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using mocha.Model;

namespace mocha.ViewModel
{
    public class CategoryViewModel : INotifyPropertyChanged
    {
        private int _id;
        private int _sort;
        private string _name;
        public int id
        {
            get { return _id; }
            set
            {
                _id = value;
                OnPropertyChanged(new PropertyChangedEventArgs("id"));
            }
        }

        public int sort
        {
            get { return _sort; }
            set
            {
                _sort = value;
                OnPropertyChanged(new PropertyChangedEventArgs("sort"));
            }
        }

        public string name
        {
            get { return _name; }
            set
            {
                _name = value;
                OnPropertyChanged(new PropertyChangedEventArgs("name"));
            }
        }

        public CategoryViewModel()
        {
            id = 0;
            name = "All";
            sort = 0;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, e);
            }
        }
    }

    public class CategoriesViewModel
    {
        public ObservableCollection<CategoryViewModel> categories { get; set; }

        public CategoriesViewModel()
        {
            categories = getData();
            if (categories.Count < 1)
                categories.Add(new CategoryViewModel()); //placeholder
        }
        /*
        * retrieve list of categories
        */
        private ObservableCollection<CategoryViewModel> getData()
        {
            ObservableCollection<CategoryViewModel> cats = new ObservableCollection<CategoryViewModel>();
            CategoryViewModel cat;
            Categories rawData = Categories.getCategories();
            cats.Add(new CategoryViewModel());
            foreach (Category row in rawData)
            {
                cat = new CategoryViewModel();
                cat.id = row.id;
                cat.sort = row.sort;
                cat.name = row.name;
                cats.Add(cat);
            }
            return cats;
        }
    }
}
