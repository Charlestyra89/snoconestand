﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace mocha.Model
{
    public class BuilderItem
    {
        long id { get; set; }
        long productId { get; set; }
        int gridRow { get; set; }
        int gridCol { get; set; }

        public BuilderItem()
        {
        }

        public BuilderItem(long _productId, int _gridRow, int _gridCol)
        {
            this.productId = _productId;
            this.gridRow = _gridRow;
            this.gridCol = _gridCol;
        }
    }
}
