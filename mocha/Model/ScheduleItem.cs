﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace mocha.Model
{
    public class ScheduleItem
    {
        public long id { get; set; }
        public DateTime startTime { get; set; }
        public DateTime endTime { get; set; }
        public long employeeId { get; set; }

        public ScheduleItem()
        {
        }

        public void commitItem()
        {
            using (var con = new SQLiteConnection(Properties.Settings.Default.conString))
            {
                using (var com = new SQLiteCommand(con))
                {
                    con.Open();
                    com.CommandText =
                        "update or ignore ScheduleItem SET employeeId = " + employeeId + ", startTime = " + startTime.ToOADate() + ", endtime = " + endTime.ToOADate() +" where id = " + id + "; " +
                        "insert or ignore into ScheduleItem (id, employeeId, startTime, endTime) Values (" + id + ", " + employeeId + ", " + startTime.ToOADate() + ", " + endTime.ToOADate() + ")";
                    com.ExecuteNonQuery();
                }
            }
        }

        public static long getItemId()
        {
            using (var con = new SQLiteConnection(Properties.Settings.Default.conString))
            {
                using (var com = new SQLiteCommand(con))
                {
                    con.Open();
                    com.CommandText =
                        "select count(id) as num from ScheduleItem";
                    using (var reader = com.ExecuteReader())
                    {
                        if (reader.Read())
                            return long.Parse(reader["num"].ToString()) + 1;
                    }
                }
            }
            return 1;
        }

    }

    public class ScheduleItems : List<ScheduleItem>
    {
        public static ScheduleItems getSchedule(DateTime start, DateTime end)
        {
            using (var con = new SQLiteConnection(Properties.Settings.Default.conString))
            {
                using (var com = new SQLiteCommand(con))
                {
                    con.Open();
                    com.CommandText =
                        "";

                }
            }
        }
    }
}
