﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace mocha.Model
{
    public class TimeClockEvent
    {
        public long id { get; set; }
        public long employeeId { get; set; }
        public int timeClockEventId { get; set; }
        public DateTime time { get; set; }

        public TimeClockEvent()
        {

        }

        public static void insertEvent(int typeId, long empId)
        {
            using (SQLiteConnection con = new SQLiteConnection(Properties.Settings.Default.conString))
            {
                string commandText =
                    "insert into TimeClockEvent (employeeId, timeClockEventId, dateAndTime) Values (" + empId.ToString() + ", " + typeId.ToString() + ", " + DateTime.Now.ToOADate() + ")";
                using (SQLiteCommand com = new SQLiteCommand(commandText, con))
                {
                    con.Open();
                    com.ExecuteNonQuery();
                }
            }
        }
    }

    public class TimeClockEvents : List<TimeClockEvent>
    {
        public static TimeClockEvents getEvents(long id)
        {
            TimeClockEvents events = new TimeClockEvents();
            using (SQLiteConnection con = new SQLiteConnection(Properties.Settings.Default.conString))
            {
                string commandText =
                    "select id, employeeId, timeClockEventId, dateAndTime from TimeClockEvent where employeeId = " + id.ToString();
                using (SQLiteCommand com = new SQLiteCommand(commandText, con))
                {
                    con.Open();
                    TimeClockEvent clockEvent;
                    using (SQLiteDataReader reader = com.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            clockEvent = new TimeClockEvent();
                            clockEvent.id = long.Parse(reader["id"].ToString());
                            clockEvent.employeeId = long.Parse(reader["employeeId"].ToString());
                            clockEvent.timeClockEventId = int.Parse(reader["timeClockEventId"].ToString());
                            clockEvent.time = DateTime.FromOADate(double.Parse(reader["dateAndTime"].ToString()));
                            events.Add(clockEvent);
                        }
                    }
                }

            }
            return events;
        }
    }
}
