﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Data.SQLite;

namespace mocha.Model
{
    public class RecipeStep
    {
        public long productId { get; set; }
        public int sort { get; set; }
        public string stepDesc { get; set; }


        public RecipeStep(string _stepDesc)
        {
            this.stepDesc = _stepDesc;
        }

        public RecipeStep()
        {

        }
    }

    public class RecipeSteps : List<RecipeStep>
    {
        public static RecipeSteps getRecipeSteps(long prodId)
        {
            RecipeSteps steps = new RecipeSteps();
            using (SQLiteConnection con = new SQLiteConnection(Properties.Settings.Default.conString))
            {
                con.Open();
                string commandText =
                    "select stepDesc, sort from RecipeStep where productId = " + prodId.ToString();
                using (SQLiteCommand com = new SQLiteCommand(commandText, con))
                {
                    using (SQLiteDataReader reader = com.ExecuteReader())
                    {
                        RecipeStep step;
                        while (reader.Read())
                        {
                            step = new RecipeStep();
                            step.sort = int.Parse(reader["sort"].ToString());
                            step.productId = prodId;
                            step.stepDesc = reader["stepDesc"].ToString();
                            steps.Add(step);
                        }
                    }
                }
            }
            return steps;
        }

        public bool updateRecipe(long prodId)
        {
            RecipeSteps oldSteps = RecipeSteps.getRecipeSteps(prodId);
            SQLiteCommand command;
            try
            {
                using (SQLiteConnection con = new SQLiteConnection(Properties.Settings.Default.conString))
                {
                    con.Open();
                    string updateText, insertText, deleteText;
                    for (int i = 0; i < this.Count(); i++)
                    {
                        updateText = "update RecipeStep Set stepDesc = '" + this[i].stepDesc + "' where productId = " + prodId.ToString() + " and sort = " + this[i].sort.ToString();
                        insertText = "insert into RecipeStep (sort, productId, stepDesc) Values (" + this[i].sort + ", " + prodId + ", '" + this[i].stepDesc + "')";
                        
                        if (oldSteps.Count >= i+1 && oldSteps[i].sort == this[i].sort)
                        {
                            command = new SQLiteCommand(updateText, con);
                            command.ExecuteNonQuery();
                        }
                        else
                        {
                            command = new SQLiteCommand(insertText, con);
                            command.ExecuteNonQuery();
                        }
                    }
                    for (int j = oldSteps.Count - 1; j > this.Count(); j--)
                    {
                        deleteText = "delete from RecipeStep where sort = " + oldSteps[j].sort.ToString() + " and stepDesc = " + oldSteps[j].stepDesc;
                        command = new SQLiteCommand(deleteText, con);
                        command.ExecuteNonQuery();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
