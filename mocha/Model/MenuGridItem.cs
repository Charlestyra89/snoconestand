﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace mocha.Model
{
    public class MenuGridItem
    {
        public int id { get; set; }
        public long productId { get; set; }
        public int gridX { get; set; }
        public int gridY { get; set; }

        public MenuGridItem(long _productId, int _gridX, int _gridY)
        {
            productId = _productId;
            gridX = _gridX;
            gridY = _gridY;
        }
        public MenuGridItem()
        { }

    }

    public class MenuGridItems : List<MenuGridItem>
    {
        public MenuGridItems()
        { }

        public static MenuGridItems getMenuGridItems()
        {
            MenuGridItems items = new MenuGridItems();
            using (SQLiteConnection con = new SQLiteConnection(Properties.Settings.Default.conString))
            {
                con.Open();
                string commandText =
                    "select id, productId, xValue, yValue from POSMenuGrid";

                using (SQLiteCommand com = new SQLiteCommand(commandText, con))
                {
                    using (SQLiteDataReader reader = com.ExecuteReader())
                    {
                        MenuGridItem item;
                        while (reader.Read())
                        {
                            item = new MenuGridItem();
                            item.id = int.Parse(reader["id"].ToString());
                            item.productId = long.Parse(reader["productId"].ToString());
                            item.gridX = int.Parse(reader["xValue"].ToString());
                            item.gridY = int.Parse(reader["yValue"].ToString());
                            items.Add(item);
                        }
                    }
                }
            }
            return items;
        }

        public bool commitMenuGridItems()
        {
            using (SQLiteConnection con = new SQLiteConnection(Properties.Settings.Default.conString))
            {
                con.Open();
                using (SQLiteCommand com = new SQLiteCommand(con))
                {
                    foreach (MenuGridItem item in this)
                    {
                        com.CommandText = "insert or replace into POSMenuGrid (id, productId, xValue, yValue) values " +
                        "((select ID from POSMenuGrid where xValue = " + item.gridX + " and yValue = " + item.gridY + "), " + item.productId + ", " + item.gridX + ", " + item.gridY + ")";
                        com.ExecuteNonQuery();
                    }
                }
            }
            return true;
        }
    }
}
