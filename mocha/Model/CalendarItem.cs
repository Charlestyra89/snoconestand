﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace mocha.Model
{
    public class CalendarItem
    {
        public int id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public DateTime startTime { get; set; }
        public DateTime endTime { get; set; }
        public int serviceId { get; set; }
        public bool paymentReceived { get; set; }

        public CalendarItem()
        { }

        public CalendarItem(int _id)
        {
            using (var con = new SQLiteConnection(Properties.Settings.Default.conString))
            {
                string conString =
                    "select id, title, description, startTime, endTime, serviceId, paymentReceived " +
                    "from CalendarItem where id = " + _id.ToString();
                using (var com = new SQLiteCommand(conString, con))
                {
                    con.Open();
                    using (var reader = com.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            id = int.Parse(reader["id"].ToString());
                            title = reader["title"].ToString();
                            description = reader["description"].ToString();
                            startTime = DateTime.FromOADate(double.Parse(reader["startTime"].ToString()));
                            endTime = DateTime.FromOADate(double.Parse(reader["endTime"].ToString()));
                            serviceId = int.Parse(reader["serviceId"].ToString());
                            paymentReceived = bool.Parse(reader["paymentReceived"].ToString());
                        }
                    }
                }
            }
        }


        public bool commit(int id = 0)
        {
            using (var con = new SQLiteConnection(Properties.Settings.Default.conString))
            {
                int result = 0, rowsResult = 0;
                string insertString =
                    "insert into CalendarItem (title, description, startTime, endTime, serviceId, paymentReceived) values " +
                    "('" + title + "', '" + description + "', " + startTime.ToOADate().ToString() + ", " + endTime.ToOADate().ToString() + ", " + serviceId.ToString() + ", " + (paymentReceived ? "1" : "0") + ")";
                string updateString =
                    "update CalendarItem set title = '" + title + "', description = '" + description + "', startTime = " + startTime.ToOADate().ToString() + ", endTime = " + endTime.ToOADate().ToString() + ", serviceId = " + serviceId.ToString() + ", paymentReceived = " + (paymentReceived ? "1" : "0") +
                    " where id = " + id.ToString();
                using (var com = new SQLiteCommand(con))
                {
                    com.CommandText =
                        "select count(id) from CalendarItem where id = " + id.ToString();
                    try
                    {
                        con.Open();
                        using (var reader = com.ExecuteReader())
                        {
                            if (id != 0 && reader.Read())
                                rowsResult = 1;
                        }
                        if (rowsResult > 0)
                        {
                            com.CommandText = updateString;
                            result = com.ExecuteNonQuery();
                        }
                        else
                        {
                            com.CommandText = insertString;
                            result = com.ExecuteNonQuery();
                        }
                        if (result > 0)
                            return true;
                        else
                            return false;
                    }
                    catch (Exception ex)
                    {
                        return false;
                    }
                }
            }
        }
    }

    public class CalendarItems : List<CalendarItem>
    {
        public CalendarItems()
        {
            DateTime defaultDT = new DateTime(0);
            getData(defaultDT, defaultDT);
        }

        public CalendarItems(DateTime _startDate, DateTime _endDate)
        {
            getData(_startDate, _endDate);
        }

        private void getData(DateTime _start, DateTime _end)
        {
            DateTime defaultDT = new DateTime(0);
            using (var con = new SQLiteConnection(Properties.Settings.Default.conString))
            {
                using (var com = new SQLiteCommand(con))
                {
                    con.Open();
                    com.CommandText =
                        "select id, title, description, startTime, endTime, serviceId, paymentReceived " +
                        "from CalendarItem";
                    if (_start != defaultDT && _end != defaultDT)
                        com.CommandText += " where startTime >= " + _start.ToOADate().ToString() + " and endTime <= " + _end.ToOADate().ToString();
                    com.CommandText += " order by startTime";
                    using (var reader = com.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var item = new CalendarItem()
                            {
                                id = int.Parse(reader["id"].ToString()),
                                title = reader["title"].ToString(),
                                description = reader["description"].ToString(),
                                startTime = DateTime.FromOADate(double.Parse(reader["startTime"].ToString())),
                                endTime = DateTime.FromOADate(double.Parse(reader["endTime"].ToString())),
                                serviceId = int.Parse(reader["serviceId"].ToString()),
                                paymentReceived = bool.Parse(reader["paymentreceived"].ToString())
                            };
                            this.Add(item);
                        }
                    }
                }
            }
        }
    }
}
