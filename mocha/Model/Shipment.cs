﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace mocha.Model
{
    public class ShipmentItem
    {
        public int id { get; set; }
        public int productId { get; set; }
        public string name { get; set; }
        public string lotId { get; set; }
        public double costPerCase { get; set; }
        public int cases { get; set; }
        public int peices { get; set; }

        public ShipmentItem()
        { }
    }

    public class ShipmentItems : List<ShipmentItem>
    {
        public ShipmentItems()
        {
        }

        public ShipmentItems(long shipmentId)
        {
            using (SQLiteConnection con = new SQLiteConnection(Properties.Settings.Default.conString))
            {
                string commandText = "select " + 
                    "i.id, " + 
                    "p.id as productId, " + 
                    "p.name, " + 
                    "i.lotId, " + 
                    "l.costPerCase, " +
                    "l.qtyPerCase, " +
                    "i.qty " + 
                    "from ShipmentItem as i " +
                    "join ProductItem as p on p.id = i.productId " + 
                    "where listId = " + shipmentId.ToString();
                using (SQLiteCommand com = new SQLiteCommand(commandText, con))
                {
                    con.Open();
                    using (SQLiteDataReader reader = com.ExecuteReader())
                    {
                        ShipmentItem item;
                        while (reader.Read())
                        {
                            item = new ShipmentItem();
                            item.id = int.Parse(reader["id"].ToString());
                            item.productId = int.Parse(reader["productId"].ToString());
                            item.name = reader["name"].ToString();
                            item.lotId = reader["lotId"].ToString();
                            item.cases = int.Parse(reader["qty"].ToString());
                            item.costPerCase = double.Parse(reader["costPerCase"].ToString());
                            item.peices = item.cases * (int.Parse(reader["qtyPerCase"].ToString()));
                            this.Add(item);
                        }
                    }
                }
            }
        }
    }

    public class Shipment
    {
        public ShipmentItems items { get; set; }
        public int shipmentId { get; set; }
        public int supplierId { get; set; }
        public string supplierName { get; set; }
        public double dateReceived { get; set; }
        public string verifiedInit { get; set; }

        public Shipment()
        {
            items = new ShipmentItems();
        }
    }

    public class Shipments : List<Shipment>
    {
        public Shipments()
        {
            using (SQLiteConnection con = new SQLiteConnection(Properties.Settings.Default.conString))
            {
                string commandText =
                    "select s.id, s.supplierId, u.name, s.timeReceived, s.verifiedInit from Shipment as s join Supplier as u on s.supplierId = u.id";
                using (SQLiteCommand com = new SQLiteCommand(commandText, con))
                {
                    con.Open();
                    using (SQLiteDataReader reader = com.ExecuteReader())
                    {
                        Shipment ship;
                        while (reader.Read())
                        {
                            ship = new Shipment();
                            ship.shipmentId = int.Parse(reader["id"].ToString());
                            ship.items = new ShipmentItems(ship.shipmentId);
                            ship.supplierId = int.Parse(reader["supplierId"].ToString());
                            ship.supplierName = reader["name"].ToString();
                            ship.dateReceived = double.Parse(reader["timeReceived"].ToString());
                            ship.verifiedInit = reader["verifiedInit"].ToString();
                            this.Add(ship);
                        }
                    }
                }
            }
        }
    }
}
