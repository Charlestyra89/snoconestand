﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace mocha.Model
{
    public class Frequency
    {
        public int id { get; set; }
        public string name { get; set; }

        public Frequency()
        { }
    }

    public class Frequencies : List<Frequency>
    {
        public Frequencies()
        {
            using (var con = new SQLiteConnection(Properties.Settings.Default.conString))
            {
                string commandText =
                    "select id, name from Frequency";
                using (var com = new SQLiteCommand(commandText, con))
                {
                    con.Open();
                    using (var reader = com.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var item = new Frequency()
                            {
                                id = int.Parse(reader["id"].ToString()),
                                name = reader["name"].ToString()
                            };
                            this.Add(item);
                        }
                    }
                }
            }
        }
    }
}
