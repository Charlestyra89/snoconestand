﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace mocha.Model
{
    public class POSTransaction
    {
        private DateTime _datetime;
        private double _oaDatetime;

        public long id { get; set; }
        public int employeeId { get; set; }
        public double subtotal { get; set; }
        public double tax { get; set; }
        public double grandtotal { get; set; }
        public double tendered { get; set; }
        public string initials { get; set; }
        public DateTime datetime
        {
            get { return _datetime; }
            set
            {
                _datetime = value;
                oaDatetime = datetime.ToOADate();
            }
        }
        public double oaDatetime
        {
            get { return _oaDatetime; }
            set
            {
                _oaDatetime = value;
            }
        }
        public int tenderType { get; set; }
        public bool isOnHold { get; set; }
        public bool isNoSaled { get; set; }
        public string notes { get; set; }
        public List<ProductItem> products { get; set; }
        public POSTransaction()
        {
            products = new List<ProductItem>();
        }

        public void commitTransaction(bool onHold, bool noSale)
        {
            using (SQLiteConnection con = new SQLiteConnection(Properties.Settings.Default.conString))
            {
                string commandText =
                    "insert into POSTransaction (employeeId, subTotal, tax, grandTotal, tendered, dateEntered, tenderType, isOnHold, isNoSaled, notes) " +
                    "Values (" + Globals.empId + ", " + subtotal + ", " + tax + ", " + grandtotal + ", " + tendered + ", " + oaDatetime + ", " + tenderType + ", " + (onHold ? "1" : "0") + ", " + (noSale ? "1" : "0") + ", " + notes + ")";
                using (SQLiteCommand com = new SQLiteCommand(commandText, con))
                {
                    con.Open();
                    com.ExecuteNonQuery();
                    long id = con.LastInsertRowId;
                    foreach (ProductItem prod in products)
                    {
                        com.CommandText =
                            "insert into TransactionItems (transactionId, productId, qty) " +
                            "Values (" + id + ", " + prod.id + ", " + prod.transQty + ")";
                        com.ExecuteNonQuery();
                    }
                }
            }
        }

        public POSTransaction getTransaction(long id)
        {
            POSTransaction trans = new POSTransaction();
            using (SQLiteConnection con = new SQLiteConnection(Properties.Settings.Default.conString))
            {
                string commandText =
                    "select id, subTotal, tax, grandTotal, tendered from PosTransaction where id = " + id.ToString();
                using (SQLiteCommand com = new SQLiteCommand(commandText, con))
                {
                    con.Open();
                    using (SQLiteDataReader reader = com.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            trans.id = long.Parse(reader["id"].ToString());
                            trans.subtotal = double.Parse(reader["subtotal"].ToString());
                            trans.tax = double.Parse(reader["tax"].ToString());
                            trans.grandtotal = double.Parse(reader["grandTotal"].ToString());
                            trans.products = new List<ProductItem>();
                        }
                    }
                    com.CommandText = "select p.id, p.name, p.price, p.image, l.qty from TransactionItems as l join ProductItem as p on l.productId = p.id where transactionId = " + id.ToString();
                    ProductItem item;
                    using (SQLiteDataReader reader = com.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            item = new ProductItem();
                            item.id = long.Parse(reader["id"].ToString());
                            item.name = reader["name"].ToString();
                            item.price = double.Parse(reader["price"].ToString());
                            item.image = reader["image"].ToString();
                            item.transQty = int.Parse(reader["qty"].ToString());
                            trans.products.Add(item);
                        }
                    }
                }
            }
            return trans;
        }


    }

    public class POSTransactions : List<POSTransaction>
    {
        public POSTransactions()
        { }

        public static POSTransactions getTransactions(bool completed, bool onHold, bool noSales, bool all)
        {
            POSTransactions transList = new POSTransactions();
            using (SQLiteConnection con = new SQLiteConnection(Properties.Settings.Default.conString))
            {
                string commandText;
                commandText =
                    "select id, subTotal, tax, grandTotal, dateEntered, isOnHold, isNoSaled, initials from PosTransaction";
                if (completed)
                    commandText += " where (isOnHold = 0 or isOnHold is null) and (isNoSaled = 0 or isNoSaled is null) and (deleted = 'false' or deleted is null)";
                else if (onHold)
                    commandText += " where isOnHold = 1 and (deleted = 'false' or deleted is null)";
                else if (noSales)
                    commandText += " where isNoSaled = 1 and (deleted = 'false' or deleted is null)";
                else if (all)
                    commandText += " where (deleted = 'false' or deleted is null)";
                using (SQLiteCommand com = new SQLiteCommand(commandText, con))
                {
                    con.Open();
                    using (SQLiteDataReader reader = com.ExecuteReader())
                    {
                        POSTransaction item;
                        while (reader.Read())
                        {
                            item = new POSTransaction();
                            item.id = long.Parse(reader["id"].ToString());
                            item.subtotal = double.Parse(reader["subtotal"].ToString());
                            item.tax = double.Parse(reader["tax"].ToString());
                            item.grandtotal = double.Parse(reader["grandTotal"].ToString());
                            item.oaDatetime = double.Parse(reader["dateEntered"].ToString());
                            item.datetime = DateTime.FromOADate(item.oaDatetime);
                            string hold = reader["isOnHold"].ToString();
                            if (hold != "")
                                item.isOnHold = bool.Parse(hold);
                            string nosale = reader["isNoSaled"].ToString();
                            if (nosale != "")
                                item.isOnHold = bool.Parse(nosale);
                            item.initials = reader["initials"].ToString();
                            transList.Add(item);
                        }
                    }
                }
            }
            return transList;
        }
    }
}
