﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mocha.Model
{
    public class TimeClockEventType
    {
        public long id { get; set; }
        public string timeClockEvent { get; set; }
        public int bitMask { get; set; }

        public TimeClockEventType()
        {

        }
    }
}
