﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace mocha.Model
{
    public class Category
    {
        public int id { get; set; }
        public int sort { get; set; }
        public string name { get; set; }

        public Category()
        {

        }
    }

    public class Categories : List<Category>
    {
        public static Categories getCategories()
        {
            Categories cats = new Categories();
            using (SQLiteConnection con = new SQLiteConnection(Properties.Settings.Default.conString))
            {
                string commandText;
                commandText =
                    "select id, sort, name from Category order by sort";
                using (SQLiteCommand com = new SQLiteCommand(commandText, con))
                {
                    con.Open();
                    Category cat;
                    using (SQLiteDataReader reader = com.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            cat = new Category();
                            cat.id = int.Parse(reader["id"].ToString());
                            cat.sort = int.Parse(reader["sort"].ToString());
                            cat.name = reader["name"].ToString();
                            cats.Add(cat);
                        }
                    }
                }
            }
            return cats;
        }
    }
}
