﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace mocha.Model
{
    public class IngredientItem
    {
        public long productId { get; set; }
        public long id { get; set; }
        public string name { get; set; }
        public int sort { get; set; }

        public IngredientItem()
        { }
    }

    public class Ingredients : List<IngredientItem>
    {
        
        public static Ingredients getIngredients(long id)
        {
            Ingredients items = new Ingredients();
            using (SQLiteConnection con = new SQLiteConnection(Properties.Settings.Default.conString))
            {
                con.Open();
                string commandText =
                    "select l.ingredientId, i.name, l.sort from ProductItem as i join IngredientList as l on i.id = l.ingredientId where l.productId = " + id.ToString() + " order by l.sort";
                using (SQLiteCommand com = new SQLiteCommand(commandText, con))
                {
                    using (SQLiteDataReader reader = com.ExecuteReader())
                    {
                        IngredientItem item;
                        while (reader.Read())
                        {
                            item = new IngredientItem();
                            item.id = long.Parse(reader["ingredientId"].ToString());
                            item.name = reader["name"].ToString();
                            item.sort = int.Parse(reader["sort"].ToString());
                            items.Add(item);
                        }
                    }
                }
            }
            return items;
        }

        public static Ingredients searchIngredients(string name)
        {
            Ingredients items = new Ingredients();
            using (SQLiteConnection con = new SQLiteConnection(Properties.Settings.Default.conString))
            {
                con.Open();
                string commandText =
                    "select id, name, description from ProductItem where name like '%" + name + "%' and isIngredient = 1";
                using (SQLiteCommand com = new SQLiteCommand(commandText, con))
                {
                    using (SQLiteDataReader reader = com.ExecuteReader())
                    {
                        
                        IngredientItem item;
                        while (reader.Read())
                        {
                            item = new IngredientItem();
                            item.id = long.Parse(reader["id"].ToString());
                            item.name = reader["name"].ToString();
                            items.Add(item);
                        }
                    }
                }
            }
            return items;
        }

        internal bool updateIngredients(long id)
        {
            Ingredients oldIng = Ingredients.getIngredients(id);
            SQLiteCommand command;
            try
            {
                using (SQLiteConnection con = new SQLiteConnection(Properties.Settings.Default.conString))
                {
                    con.Open();
                    string updateText, insertText, deleteText;
                    for (int i = 0; i < this.Count(); i++)
                    {
                        updateText = "update IngredientList Set " + 
                            "ingredientId = " + this[i].id.ToString() + " " +
                            " where productId = " + id.ToString() + " and sort = " + this[i].sort.ToString();
                        insertText = "insert into IngredientList " + 
                            "(productId, ingredientId, sort) " + 
                            "Values (" + id.ToString() + ", " + this[i].id.ToString() + ", " + this[i].sort.ToString() + ")";
                        if (oldIng.Count >= i+1 && oldIng[i].sort == this[i].sort)
                        {
                            command = new SQLiteCommand(updateText, con);
                            command.ExecuteNonQuery();
                        }
                        else
                        {
                            command = new SQLiteCommand(insertText, con);
                            command.ExecuteNonQuery();
                        }
                    }
                    for (int j = oldIng.Count - 1; j > this.Count(); j--)
                    {
                        deleteText = "delete from IngredientList where sort = " + oldIng[j].sort.ToString() + " and productId = " + id;
                        command = new SQLiteCommand(deleteText, con);
                        command.ExecuteNonQuery();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
