﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace mocha.Model
{
    public class Supplier
    {
        public long id { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string phone { get;set; }
        public string email { get; set; }
        public string notes { get; set; }

        public Supplier()
        {}

    }

    public class Suppliers : List<Supplier>
    {
        public static Suppliers getSuppliers()
        {
            Suppliers sups = new Suppliers();
            using (SQLiteConnection con = new SQLiteConnection(Properties.Settings.Default.conString))
            {
                con.Open();
                string commandText = "select id, name, address, phone, email, notes from Supplier";
                using (SQLiteCommand com = new SQLiteCommand(commandText, con))
                {
                    using (SQLiteDataReader reader = com.ExecuteReader())
                    {
                        Supplier sup;
                        while (reader.Read())
                        {
                            sup = new Supplier();
                            sup.id = long.Parse(reader["id"].ToString());
                            sup.name = reader["name"].ToString();
                            sup.address = reader["address"].ToString();
                            sup.phone = reader["phone"].ToString();
                            sup.email = reader["email"].ToString();
                            sup.notes = reader["notes"].ToString();
                            sups.Add(sup);
                        }
                    }
                }
            }
            return sups;
        }

        public static Supplier getSupplier(long id)
        {
            Supplier sup = null;
            using (SQLiteConnection con = new SQLiteConnection(Properties.Settings.Default.conString))
            {
                con.Open();
                string commandText = "select id, name, address, phone, email, notes from Supplier where id = " + id.ToString();
                using (SQLiteCommand com = new SQLiteCommand(commandText, con))
                {
                    using (SQLiteDataReader reader = com.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            sup = new Supplier();
                            sup.id = long.Parse(reader["id"].ToString());
                            sup.name = reader["name"].ToString();
                            sup.address = reader["address"].ToString();
                            sup.phone = reader["phone"].ToString();
                            sup.email = reader["email"].ToString();
                            sup.notes = reader["notes"].ToString();
                        }
                    }
                }
            }
            return sup;
        }
    }
}
