﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace mocha.Model
{
    public class TenderType
    {

        public int id { get; set; }
        public string description { get; set; }

        public TenderType()
        { }
    }

    public class TenderTypes : List<TenderType>
    {
        public static TenderTypes getTypes()
        {
            TenderTypes types = new TenderTypes();
            using (SQLiteConnection con = new SQLiteConnection(Properties.Settings.Default.conString))
            {
                string commandText =
                    "select id, description from TenderType where deleted is null or deleted = 0";
                using (SQLiteCommand com = new SQLiteCommand(commandText, con))
                {
                    con.Open();
                    using (SQLiteDataReader reader = com.ExecuteReader())
                    {
                        TenderType type;
                        while (reader.Read())
                        {
                            type = new TenderType();
                            type.id = int.Parse(reader["id"].ToString());
                            type.description = reader["description"].ToString();
                            types.Add(type);
                        }

                    }
                }
            }
            return types;
        }
    }
}
