﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mocha.Model
{
    public class RecipeList
    {
        public long id { get; set; }
        public long sort { get; set; }
        public long receiptItemId { get; set; }

        public RecipeList()
        {

        }
    }
}
