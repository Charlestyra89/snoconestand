﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace mocha.Model
{
    public class ProductItem
    {
        private double _dateAdded;
        private DateTime _dateDisplayed;
        public long id { get; set; }
        public string name { get; set; }
        public double price { get; set; }
        public long ingredientListId { get; set; }
        public string description { get; set; }
        public bool hasRecipe { get; set; }
        public int transQty { get; set; }
        public string image { get; set; }
        public bool isForSale { get; set; }

        public double dateAdded
        {
            get { return _dateAdded; }
            set
            {
                _dateAdded = value;
            }
        }
        public DateTime dateDisplayed
        {
            get { return DateTime.FromOADate(_dateAdded); }
            set
            {
                _dateDisplayed = value;
                dateAdded = dateDisplayed.ToOADate();
            }
        }
        public int categoryId { get; set; }
        public bool isIngredient { get; set; }

        public ProductItem()
        {

        }

        public ProductItem(long _id, int _qty)
        {
            id = _id;
            transQty = _qty;
        }

        public bool updateProduct()
        {
            using (SQLiteConnection con = new SQLiteConnection(Properties.Settings.Default.conString))
            {
                using (SQLiteCommand com = new SQLiteCommand(con))
                {
                    con.Open();
                    com.CommandText = "update ProductItem " +
                    "Set name = '" + this.name + "', " +
                    "price = " + this.price + ", " +
                    "description = '" + this.description + "', " +
                    "hasRecipe = " + (this.hasRecipe == true ? "1" : "0") + ", " +
                    "dateAdded = " + this.dateAdded + ", " +
                    "categoryId = " + this.categoryId +
                    " where id = " + this.id.ToString();
                    int rows = com.ExecuteNonQuery();
                    if (rows > 0)
                        return true;
                    else
                        return false;
                }
            }
        }

        public static ProductItem getProduct(long prodId)
        {
            ProductItem item = new ProductItem();
            using (SQLiteConnection con = new SQLiteConnection(Properties.Settings.Default.conString))
            {
                con.Open();
                string commandText;
                commandText =
                    "select id, name, price, description, hasRecipe, dateAdded, categoryId, isIngredient, isForSale, image " +
                    "from ProductItem where (deleted = 0 or deleted is NULL) and id = " + prodId.ToString();
                using (SQLiteCommand com = new SQLiteCommand(commandText, con))
                {
                    using (SQLiteDataReader reader = com.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            item.id = long.Parse(reader["id"].ToString());
                            item.name = reader["name"].ToString();
                            item.price = float.Parse(reader["price"].ToString());
                            item.description = reader["description"].ToString();
                            item.hasRecipe = bool.Parse(reader["hasRecipe"].ToString());
                            item.dateAdded = Double.Parse(reader["dateAdded"].ToString());
                            item.categoryId = int.Parse(reader["categoryId"].ToString());
                            item.isIngredient = bool.Parse(reader["isIngredient"].ToString());
                            item.isForSale = bool.Parse(reader["isForSale"].ToString());
                            item.image = reader["image"].ToString();
                        }
                    }
                }
            }
            return item;
        }

        public static string insertProduct(ProductItem item)
        {
            string result = "";
            using (SQLiteConnection con = new SQLiteConnection(Properties.Settings.Default.conString))
            {
                string commandText =
                    "insert into ProductItem (name, price, description, hasRecipe, dateAdded, categoryId, isIngredient) " +
                    "Values('" + item.name + "', " + item.price + ", " + item.description + "', " + (item.hasRecipe ? "1" : "0") + ", " + DateTime.Now.ToOADate() + ", " + item.categoryId + ", " + (item.isIngredient ? "1" : "0") + ")";
                using (SQLiteCommand com = new SQLiteCommand(commandText, con))
                {
                    try
                    {
                        con.Open();
                        com.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        result = "Unable to add product! " + ex.Message;
                    }
                }
            }
            return result;
        }

        public static string deleteProduct(long selectedId)
        {
            using (SQLiteConnection con = new SQLiteConnection(Properties.Settings.Default.conString))
            {
                string commandText =
                    "update productItem set deleted = 1 where id = " + selectedId.ToString();
                using (SQLiteCommand com = new SQLiteCommand(commandText, con))
                {
                    try
                    {
                        con.Open();
                        com.ExecuteNonQuery();
                        return "";
                    }
                    catch (Exception ex)
                    {
                        return "Unable to delete product item! " + ex.Message;
                    }
                }
            }
        }
    }

    public class Products : List<ProductItem>
    {
        public static Products getProducts(int catId)
        {
            Products prods = new Products();
            using (SQLiteConnection con = new SQLiteConnection(Properties.Settings.Default.conString))
            {
                string commandText;
                commandText =
                    "select id, name, price, description, hasRecipe, dateAdded, categoryId, isIngredient, image " +
                    "from ProductItem where deleted = 0 or deleted is NULL";
                if (catId > 0)
                    commandText += " and categoryId = " + catId.ToString();
                using (SQLiteCommand com = new SQLiteCommand(commandText, con))
                {
                    con.Open();
                    using (SQLiteDataReader reader = com.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var item = new ProductItem()
                            {
                                id = long.Parse(reader["id"].ToString()),
                                name = reader["name"].ToString(),
                                price = float.Parse(reader["price"].ToString()),
                                description = reader["description"].ToString(),
                                hasRecipe = bool.Parse(reader["hasRecipe"].ToString()),
                                //string str = reader["dateAdded"].ToString();
                                dateAdded = Double.Parse(reader["dateAdded"].ToString()),
                                categoryId = int.Parse(reader["categoryId"].ToString()),
                                isIngredient = bool.Parse(reader["isIngredient"].ToString()),
                                image = reader["image"].ToString()
                            };
                            prods.Add(item);
                        }
                    }
                }
            }
            return prods;
        }
    }
}
