﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace mocha.Model
{
    public class Employee
    {
        public long id { get; set; }
        public string lastName { get; set; }
        public string firstName { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string addressLine1 { get; set; }
        public string addressLine2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public float payRate { get; set; }
        public string userName { get; set; }
        public string pin { get; set; }
        public bool isManager { get; set; }
        public int clockStatus { get; set; }

        public Employee()
        {

        }

        public Employee(string lastName, string firstName)
        {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        public static void commitEmployee(Employee emp)
        {
            using (SQLiteConnection con = new SQLiteConnection(Properties.Settings.Default.conString))
            {
                string commandText;
                if (emp.id == 0)
                {
                    commandText = "insert into Employee(lastName, firstName, phone, email, addressLine1, addressLine2, city, state, payRate, userName, pin, isManager, zip) " +
                        "Values('" + emp.lastName + "', '" + emp.firstName + "', '" + emp.phone + "', '" + emp.email + "', '" + emp.addressLine1 + "', '" + emp.addressLine2 + "', '" +
                        emp.city + "', '" + emp.state + "', " + emp.payRate.ToString() + ", '" + emp.userName + "', '" + emp.pin + "', " + (emp.isManager == true ? "1" : "0") + ", '" + emp.zip + "')";
                }
                else
                {
                    commandText = "update Employee Set lastName = '" + emp.lastName + "', firstName = '" + emp.firstName + "', phone = '" + emp.phone + "', email = '" + emp.email + "', addressLine1 = '" + emp.addressLine1 + "', addressLine2 = '" + emp.addressLine2 + "', city = '" + emp.city + "', state = '" + emp.state + "', userName='" + emp.userName + "', payRate = " + emp.payRate + ", pin = '" + emp.pin + "', isManager = " + (emp.isManager == true ? "1" : "0") + ", zip = '" + emp.zip + "' where id = " + emp.id.ToString();
                }
                using (SQLiteCommand com = new SQLiteCommand(commandText, con))
                {
                    try
                    {
                        con.Open();
                        com.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
        }

        public static void deleteEmployee(long id)
        {
            using (SQLiteConnection con = new SQLiteConnection(Properties.Settings.Default.conString))
            {
                string commandText =
                    "update Employee set deleted = 1 where id = " + id.ToString();
                using (SQLiteCommand com = new SQLiteCommand(commandText, con))
                {
                    con.Open();
                    com.ExecuteNonQuery();
                }
            }
        }

        public static Employee getEmployee(long id)
        {
            Employee result = null;
            using (SQLiteConnection con = new SQLiteConnection(Properties.Settings.Default.conString))
            {
                string commandText =
                    "select id, lastName, firstName, phone, email, addressLine1, addressLine2, city, " +
                    "state, zip, payRate, userName, pin, isManager, clockStatus from Employee where id = " + id.ToString();
                using (SQLiteCommand com = new SQLiteCommand(commandText, con))
                {
                    con.Open();
                    using (SQLiteDataReader reader = com.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            result = new Employee();
                            result.id = long.Parse(reader["id"].ToString());
                            result.lastName = reader["lastName"].ToString();
                            result.firstName = reader["firstName"].ToString();
                            result.phone = reader["phone"].ToString();
                            result.email = reader["email"].ToString();
                            result.addressLine1 = reader["addressLine1"].ToString();
                            result.addressLine2 = reader["addressLine2"].ToString();
                            result.city = reader["city"].ToString();
                            result.state = reader["state"].ToString();
                            result.zip = reader["zip"].ToString();
                            result.payRate = float.Parse(reader["payRate"].ToString());
                            result.userName = reader["userName"].ToString();
                            result.pin = reader["pin"].ToString();
                            result.isManager = bool.Parse(reader["isManager"].ToString());
                            result.clockStatus = int.Parse(reader["clockStatus"].ToString());
                        }
                    }
                }
            }
            return result;
        }

        public static long tryLogin(string userName, string pin)
        {
            long id = -1;
            using (SQLiteConnection con = new SQLiteConnection(Properties.Settings.Default.conString))
            {
                string commandText =
                    "select id from Employee where userName = '" + userName + "' and pin = '" + pin + "'";
                using (SQLiteCommand com = new SQLiteCommand(commandText, con))
                {
                    con.Open();
                    using (SQLiteDataReader reader = com.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            id = long.Parse(reader["id"].ToString());
                        }
                    }
                }
            }
            return id;
        }

        public static int updateClockStatus(int typeId, Employee emp)
        {
            using (SQLiteConnection con = new SQLiteConnection(Properties.Settings.Default.conString))
            {
                con.Open();
                int bitMask = 0;
                using (SQLiteCommand com1 = new SQLiteCommand(con))
                {
                    com1.CommandText = 
                        "select bitMask from TimeClockEventType where id = " + typeId.ToString();
                    using (SQLiteDataReader reader = com1.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            bitMask = int.Parse(reader["bitMask"].ToString());
                        }
                    }
                }
                
                if (bitMask == 4)
                    emp.clockStatus = 0;
                else
                    emp.clockStatus = bitMask;
                string commandText =
                    "update Employee set clockStatus =  " + emp.clockStatus.ToString() + " where id = " + emp.id.ToString();
                using (SQLiteCommand com2 = new SQLiteCommand(commandText, con))
                {
                    com2.ExecuteNonQuery();
                }
            }
            return emp.clockStatus;
        }
    }

    public class Employees : List<Employee>
    {
        public static Employees getEmployees()
        {
            Employees results = new Employees();
            try
            {
                using (SQLiteConnection con = new SQLiteConnection(Properties.Settings.Default.conString))
                {
                    string commandText =
                        "select id, lastName, firstName, phone, email, addressLine1, addressLine2, city, " +
                        "state, zip, payRate, userName, pin, isManager, clockStatus from Employee where deleted = 0 or deleted is NULL";
                    using (SQLiteCommand com = new SQLiteCommand(commandText, con))
                    {
                        con.Open();
                        Employee emp;
                        using (SQLiteDataReader reader = com.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                emp = new Employee();
                                emp.id = long.Parse(reader["id"].ToString());
                                emp.lastName = reader["lastName"].ToString();
                                emp.firstName = reader["firstName"].ToString();
                                emp.phone = reader["phone"].ToString();
                                emp.email = reader["email"].ToString();
                                emp.addressLine1 = reader["addressLine1"].ToString();
                                emp.addressLine2 = reader["addressLine2"].ToString();
                                emp.city = reader["city"].ToString();
                                emp.state = reader["state"].ToString();
                                emp.zip = reader["zip"].ToString();
                                emp.payRate = float.Parse(reader["payRate"].ToString());
                                emp.userName = reader["userName"].ToString();
                                emp.pin = reader["pin"].ToString();
                                emp.isManager = bool.Parse(reader["isManager"].ToString());
                                emp.clockStatus = (reader["clockStatus"].ToString()==""?0:int.Parse(reader["clockStatus"].ToString()));
                                results.Add(emp);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return results;

        }
    }
}
