﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.ComponentModel;

namespace mocha.Model
{
    public class ProductLot : INotifyPropertyChanged
    {
        private bool _use;
        private double _qty;
        private double _costPerCase;
        private int _qtyPerCase;
        private double _dateAdded;
        private DateTime _displayDate;
        private double _expires;
        private DateTime _displayExpires;
        private int _supplierId;
        private string _notes;
        private double _costPerUnit;

        public long id { get; set; }
        public bool use 
        {
            get { return _use; }
            set
            {
                _use = value;
                OnPropertyChanged(new PropertyChangedEventArgs("use"));
            }
        }
        public long productId { get; set; }
        public double qty
        {
            get { return _qty; }
            set
            {
                _qty = value;
                OnPropertyChanged(new PropertyChangedEventArgs("qty"));
            }
        }
        public double costPerCase
        {
            get { return _costPerCase; }
            set
            {
                _costPerCase = value;
                OnPropertyChanged(new PropertyChangedEventArgs("costPerCase"));
                costPerUnit = costPerCase / qtyPerCase;
            }
        }
        public int qtyPerCase
        {
            get { return _qtyPerCase; }
            set
            {
                _qtyPerCase = value;
                OnPropertyChanged(new PropertyChangedEventArgs("qtyPerCase"));
                costPerUnit = costPerCase / qtyPerCase;
            }
        }
        public double dateAdded 
        {
            get { return _dateAdded; }
            set
            {
                _dateAdded = value;
                OnPropertyChanged(new PropertyChangedEventArgs("dateAdded"));
            }
        }
        public DateTime displayDate
        {
            get { return DateTime.FromOADate(_dateAdded); }
            set
            {
                _displayDate = value;
                dateAdded = _displayDate.ToOADate();
                OnPropertyChanged(new PropertyChangedEventArgs("displayDate"));
            }
        }
        public double expires
        {
            get { return _expires; }
            set
            {
                _expires = value;
                OnPropertyChanged(new PropertyChangedEventArgs("expires"));
            }
        }
        public DateTime displayExpires
        {
            get { return DateTime.FromOADate(_expires); }
            set
            {
                _displayExpires = value;
                expires = _displayExpires.ToOADate();
                OnPropertyChanged(new PropertyChangedEventArgs("displayExpires"));
            }
        }
        public int supplierId
        {
            get { return _supplierId; }
            set
            {
                _supplierId = value;
                OnPropertyChanged(new PropertyChangedEventArgs("supplierId"));
            }
        }
        public string notes
        {
            get { return _notes; }
            set
            {
                _notes = value;
                OnPropertyChanged(new PropertyChangedEventArgs("notes"));
            }
        }
        public double costPerUnit
        {
            get { return _costPerUnit; }
            set
            {
                _costPerUnit = value;
                OnPropertyChanged(new PropertyChangedEventArgs("costPerUnit"));
            }
        }

        public ProductLot()
        { }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, e);
            }
        }
        
    }

    public class ProductLots : List<ProductLot>
    {
        public static ProductLots getLots(long productId)
        {
            ProductLots lots = new ProductLots();
            using (SQLiteConnection con = new SQLiteConnection(Properties.Settings.Default.conString))
            {
                string commandText =
                    "select id, qty, costPerCase, qtyPerCase, dateAdded, expires, supplierId, notes from " +
                    "ProductLot where productId = " + productId.ToString();
                using (SQLiteCommand com = new SQLiteCommand(commandText, con))
                {
                    con.Open();
                    using (SQLiteDataReader reader = com.ExecuteReader())
                    {
                        ProductLot lot;
                        while (reader.Read())
                        {
                            lot = new ProductLot();
                            lot.id = long.Parse(reader["id"].ToString());
                            lot.qty = double.Parse(reader["qty"].ToString());
                            lot.costPerCase =double.Parse( reader["costPerCase"].ToString());
                            lot.qtyPerCase = int.Parse(reader["qtyPerCase"].ToString());
                            lot.dateAdded = double.Parse(reader["dateAdded"].ToString() == "" ? "0" : reader["dateAdded"].ToString());
                            lot.expires = double.Parse(reader["expires"].ToString() == "" ? "0" : reader["expires"].ToString());
                            lot.supplierId = int.Parse(reader["supplierId"].ToString());
                            lot.notes = reader["notes"].ToString();
                            lots.Add(lot);
                        }
                    }
                }
            }
            return lots;
        }

        internal string commitChanges()
        {
            using (SQLiteConnection con = new SQLiteConnection(Properties.Settings.Default.conString))
            {
                using (SQLiteCommand com = new SQLiteCommand(con))
                {
                    con.Open();
                    foreach (ProductLot lot in this)
                    {
                        try
                        {
                            com.CommandText =
                                "update ProductLot set use = " + (lot.use ? "1" : "0") + "where id = " + lot.id.ToString() + " and productId = " + lot.productId.ToString();
                            com.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            return ex.Message;
                        }
                    }
                }
            }
            return "";
        }
    }
}
